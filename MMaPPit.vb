Imports System.Drawing
Public Class MMaPPit
    'hexmap2 is intended to work with multiple layers and generic input files...

#Region "Declarations"
    Dim bHaveMouse As Boolean
    Dim ptOriginal As Point
    Dim ptLast As Point

    Dim MapFileFN As String

    '
    'Dim Ncells As Integer
    Dim MxStandID As Integer = 0
    Dim NColors As Integer
    Dim LegendColor() As ColorInfo
    Dim iStandColor(,) As Integer 'stand color by stand, window, and stage
    Dim bColorFile As Boolean 'whether a color file is defined
    'Dim GRIDPolys() As PolyInfo

    Dim GRIDPolys_PolyID() As Integer
    Dim GRIDPolys_DisplayID() As String
    Dim GRIDPolys_QueryID() As String
    Dim GRIDPolys_IdentifyID() As String
    'Dim StandValues() As String
    Dim NStandValues As Integer

    'Dim GRIDPolys_ColorInd() As Integer '() As Integer 'index to "StandInfo" Color by window
    Dim GRIDPolys_xVal() As Integer
    Dim GRIDPolys_yVal() As Integer
    Dim GRIDPolys_Edge(,) As Byte
    Dim GRIDPolys_QueryEdge(,) As Byte
    Dim GRIDPolys_AdjCellInd(,) As Integer

    Dim Windows_MinX() As Double
    Dim Windows_MinY() As Double
    Dim Windows_MaxX() As Double
    Dim Windows_MaxY() As Double
    Dim Windows_Value(,) As Single 'the value of the stand in the given window

    Dim Windows_NWindStands() As Integer
    Dim Windows_Label() As String
    Dim Windows_Period() As Integer 'the time period of the window - fill if can detect, otherwise default to 1-X

    Dim Acres() As Single
    'Dim bShowAcres As Boolean
    Dim Windows_Acres(,) As Single 'by window, value for that window


    Dim WindowValue() As String 'corresponding label for the entry - the file can contain multiple species all covering the same timesteps
    Dim WindowTimeStep() As Integer 'timestep of the 
    Dim FullMap As MapInfo 'original coordinates of the input map
    Dim DispMap As MapInfo 'extent of the display expressed in coordinates of the orignal map
    Dim PolyIndByPolyID() As Integer 'put in StandID and output hex ID for polygon array
    'Dim StandIndByStandID() As Integer 'put in StandID and get the kstand index

    'variables that describe the windows...
    Dim NWindows As Integer
    'Dim NAtts As Integer 'number of unique attribute titles
    Dim CurrWindInd As Integer 'window index of the currently displayed window
    Dim bDrawEdges As Boolean 'whether you want edges shown on the map or not    
    Dim bDrawFireEvents As Boolean 'whether you want to outline fire events or not
    Dim bDrawNewEdges As Boolean 'whether you have identified new stand boundaries to draw


    Dim FullExtBitmap As System.Drawing.Bitmap
    Dim PaintBitMap As System.Drawing.Bitmap
    Dim DisplayEdgeBitMap As System.Drawing.Bitmap
    Dim WindStageBitMap() As System.Drawing.Bitmap 'by window...keep good draws to reduce "play" time on subsequent plays
    Dim FullProgramBitMap() As System.Drawing.Bitmap 'by window - captures the whole MMaPPit program
    Dim FPBitmap As System.Drawing.Bitmap 'this instance of the bitmap
    Dim bWindStage As Boolean 'if this is true, you are drawing/playing the different windows and stages...if false, you are navigating
    Dim bPainted() As Boolean 'by window...if it's already been painted, then don't re-paint
    Dim BitMapExtent() As MapInfo 'the x/y coordinates of the stored bitmap to compare with the current window..
    Dim IDBitMap As System.Drawing.Bitmap 'for flashing selected/identified stands
    Dim SelBitMap As System.Drawing.Bitmap

    Dim bFlashSel As Boolean = False
    'Dim DelayTimeID As Single
    Dim PlayDelayTime As Single = 0.5 'in seconds

    'for storing map history...
    Dim MapHistory() As MapInfo
    Dim MaxHistory As Integer = 10
    Dim CurrHistInd As Integer = 1

    Dim bmpX1 As Single
    Dim bmpX2 As Single
    Dim bmpY1 As Single
    Dim bmpY2 As Single

    'grid variables
    Dim BiggestRow As Long
    Dim BiggestCol As Long
    Dim GridSlink(,) As Long 'Slink in a col,row - dimensioned by biggestrow, biggestcol


    'for Storing Attribute information
    Dim PolyAtts(,) As String 'indexed by number of polygons, number of atts
    Dim attfrm As New frmStandAtts


    'fire event stuff
    Dim FireEvent(,) As Integer 'by SLINK, time period
    Dim FireEventEdge(,) As Byte 'by SLINK, edge - whether two adjacent polys are part of the same fire event or not
    Dim FireOrigin() As Boolean 'by SLINK - indicates whether the poly should be highlighted as the origin
    Dim FireEventEdgeQual As Integer  '1 or -1 to signify whether you have an x/y swap or not. Set to 1 as a default

    'Selection Stuff
    Dim bSelected As Boolean = False ' if true, you have selected polygons to paint
    Dim iSelID() As String

    'Identify stuff
    Dim iIdnID As String


    Structure MapInfo
        Dim MinX As Double
        Dim MinY As Double
        Dim MaxX As Double
        Dim MaxY As Double
        Dim xScale As Double 'proportion of the x range of the base map used in the display
        Dim yScale As Double 'proportion of the y range of the base map used in the display
        Dim CellWidth As Double
        'Dim AngleBetweenCenters As Double
        Dim bDrawEdges As Boolean
        Dim bDrawFireEdges As Boolean
    End Structure


    Dim Play As Boolean
#End Region
    'Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
    Private Sub HexMap2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bIDClosed = True
        bMapLoaded = False
        bDrawEdges = cbDrawEdge.Checked
        bDrawFireEvents = cbFireEvents.Checked
        bDrawNewEdges = False
        'tsbcbbStandPoly.Text = tsbcbbStandPoly.Items(0)
    End Sub

    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim response As MsgBoxResult
        response = MsgBox("Do you want to close?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirm")
        If response = MsgBoxResult.Yes Then
            'My.Computer.Clipboard.Clear()
            Me.Dispose()
        ElseIf response = MsgBoxResult.No Then
            e.Cancel = True
            Exit Sub
        End If
    End Sub
    Private Sub AddTheme()
        'If bColorFile = True Then 
        If GridFileType = "XY" Then LoadWindowColorInfo(ColorIndexFile)
        ReDim MapHistory(MaxHistory)
        CurrHistInd = 1
        tsbPrev.Enabled = False
        CurrWindInd = 0
        If GridFileType = "GRID" Then CurrWindInd = 1

        Play = False

        Button1.Image = ImageList2.Images(0)
        If bMapLoaded = False Then
            LoadStaticMetricsFile(MapPolyFile)
            If GridFileType = "XY" Then LoadByPeriodData(OutputByPeriodFile, 1)
            If GridFileType = "SIMPPLLE" Then
                LoadEVUSIMData(OutputByPeriodFile, DispAtt, 1)
                ColorIndexFile = System.IO.Path.GetDirectoryName(OutputByPeriodFile) & "\" & FName(OutputByPeriodFile) & "_" & DispAtt & ".clr"
                If System.IO.File.Exists(ColorIndexFile) = False Then
                    MakeEVUSimDataColorFile(System.IO.Path.GetDirectoryName(OutputByPeriodFile) & "\" & DispAtt.Substring(0, DispAtt.Length - 3) & ".csv")
                Else
                    bColorFile = True
                    LoadWindowColorInfo(ColorIndexFile)
                    ReDefineMapColors()
                    ListView1.SelectedItems.Clear()
                End If
            End If

        End If



        ReDim bPainted(NWindows)
        ReDim WindStageBitMap(NWindows), FullProgramBitMap(NWindows)
        ReDim BitMapExtent(NWindows)
        BitMapExtent(CurrWindInd) = DispMap

        bWindStage = True
        tbCurrWindInd.Text = CurrWindInd
        Label4.Text = Windows_Label(CurrWindInd) '(CurrWindInd)
        rtbAcres.Text = ""
        For jcol As Integer = 1 To NColors
            If Windows_Acres(CurrWindInd, jcol) > 0 Then
                rtbAcres.Text = rtbAcres.Text & LegendColor(jcol).LabelDispStr & Chr(13)
                rtbAcres.Text = rtbAcres.Text & "  " & Math.Round(Windows_Acres(CurrWindInd, jcol), 0) & Chr(13)
            End If
        Next 'jatt



    End Sub

    Private Sub LoadWindowColorInfo(ByVal colorindexfil As String)
        On Error GoTo ErrHandler

        Dim dummy As String
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        Dim arr() As String
        Dim fnum As Integer

        Dim kcolm

        If bColorFile Then
            fnum = FreeFile()
            FileOpen(fnum, colorindexfil, OpenMode.Input)
            dummy = LineInput(fnum)
            NColors = Trim(LineInput(fnum))
        Else
            NColors = 0
        End If


        ' NUnqColms = 0
        'ReDim ColmName(NUnqColms), MaxLabelAttValue(NUnqColms)

        ReDim LegendColor(NColors) ', DrawColor(NdrawColors)
        'non-defined is LIGHT gray
        LegendColor(0).Color = Color.FromArgb(255, 200, 200, 200)
        LegendColor(0).LabelDispStr = "Not Defined"
        LegendColor(0).LabelMinVal = 0
        LegendColor(0).LabelMaxVal = 0

        If bColorFile Then dummy = LineInput(fnum) 'header for the colors
        For j As Integer = 1 To NColors
            'If j = 817 Then Stop
            dummy = LineInput(fnum)
            arr = Split(dummy, ",")
            LegendColor(j).LabelColorInd = Trim(arr(0))

            LegendColor(j).LabelDispStr = Trim(arr(2))
            LegendColor(j).LabelMinVal = Trim(arr(3))
            LegendColor(j).LabelMaxVal = Trim(arr(4))
            If (LegendColor(j).LabelMaxVal = LegendColor(j).LabelMinVal Or j = NColors) Then LegendColor(j).LabelMaxVal = LegendColor(j).LabelMaxVal + 0.001
            r = CType(arr(5), Integer)
            g = CType(arr(6), Integer)
            b = CType(arr(7), Integer)
            LegendColor(j).Color = Color.FromArgb(255, r, g, b)
        Next
        FileClose(fnum)


        'code for creating new legend items and populating the imagelist...
        Dim NewBitMap As System.Drawing.Bitmap
        Dim gr As System.Drawing.Graphics

        ImageList1.Images.Clear()
        ListView1.Items.Clear()
        For jcolor As Integer = 0 To NColors 'NdrawColors
            NewBitMap = New Bitmap(16, 16, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
            gr = Graphics.FromImage(NewBitMap)
            gr.FillRectangle(New SolidBrush(LegendColor(jcolor).Color), New Rectangle(0, 7, 16, 16))
            ImageList1.Images.Add(NewBitMap)
            ListView1.Items.Add(LegendColor(jcolor).LabelDispStr, jcolor)
        Next jcolor
        Exit Sub
ErrHandler:
        bColorFile = False
        FileClose(fnum)
        MsgBox("Error Reading the Color File")



    End Sub

    Private Sub LoadByPeriodData(ByVal StandOutputFile As String, ByVal WindowNumber As Integer)
        'in the future, prompt with a selector which fields contain info.

        Dim PerOutValue As Single = 0 'the output value for the stand in each period
        Dim k As Integer
        Dim StID As Integer = 0

        Dim tind As Integer = 0
        Dim kwind As Integer
        Dim kval As Integer
        Dim kcolval As Integer

        Dim CellWidth As Double

        Dim dummy As String
        Dim Atts() As String
        Dim atts2() As String
        Dim TempUniqueValues() As Single


        Dim begtic As Integer = 0
        Dim begwind As Integer = 0

        NWindows = 0

        '-------------------------------------------
        'read windows file and fill info. about number of time periods and number of stands

        NWindows = 0
        Dim Lines = IO.File.ReadAllLines(StandOutputFile)
        Atts = Split(Lines(0), ",")
        'count the tics as well

        'If PolyFileType = "XY" Then
        NWindows = Atts.Length - 2
        ReDim Windows_Label(NWindows), Windows_Period(NWindows)
        For j As Integer = 0 To NWindows
            Windows_Label(j) = Atts(j + 1)
            atts2 = Split(Windows_Label(j), "_")
            If atts2.Length > 1 Then
                Windows_Period(j) = CType(atts2(atts2.Length - 1), Integer)
            Else
                Windows_Period(j) = j
            End If
        Next

        tind = -1
        'If PolyFileType = "GRID" Then tind = 0
        'should really search back for first non-null line...
        k = Lines.length - 1
        Do
            If Trim(Lines(k)).Length > 0 Then
                MxStandID = k
                Exit Do
            End If
            k = k - 1
            If k = 0 Then Exit Do
        Loop
 

        ReDim Windows_MinX(NWindows), Windows_MinY(NWindows), Windows_MaxX(NWindows) 'StandIndByStandID(MxStID),
        ReDim Windows_MaxY(NWindows), Windows_NWindStands(NWindows)
        ReDim Windows_Acres(NWindows, NColors) ', Stands(NStands)
        ReDim Windows_Value(NWindows, MxStandID) ', Windows_WindowColorInd(NWindows, NStands)

        For jwind As Integer = begwind To NWindows
            Windows_NWindStands(jwind) = MxStandID
        Next


        'read windows file again and fill period-by-period value
        ReDim iStandColor(NWindows, MxStandID)
        ReDim Unique_Values(MxStandID)

        NUniqueValues = 0
        Unique_Values(NUniqueValues) = 0

        For jstand As Integer = 1 To MxStandID
            dummy = Lines(jstand) 'LineInput(fnum)
            Atts = Split(dummy, ",")
            StID = CType(Atts(0), Integer)
 
            For jwind As Integer = begwind To NWindows

                kwind = jwind
                PerOutValue = Math.Round(CType(Atts(kwind + 1), Single), 3)
                kcolval = GetSingleIndex(Unique_Values, PerOutValue)
                If kcolval < 0 Or NUniqueValues = 0 Then
                    NUniqueValues = NUniqueValues + 1
                    kcolval = NUniqueValues
                    Unique_Values(NUniqueValues) = PerOutValue
                End If
                Windows_Value(jwind, StID) = PerOutValue
                iStandColor(jwind, StID) = 0 'default

                'find kval
                kval = 0 'default
                For jcol As Integer = 1 To NColors
                    If PerOutValue >= LegendColor(jcol).LabelMinVal And PerOutValue < LegendColor(jcol).LabelMaxVal Then
                        kval = LegendColor(jcol).LabelColorInd
                        'also need to populate the LabelValue on this....
                        iStandColor(jwind, jstand) = jcol
                        Exit For
                    End If
                Next jcol
                Windows_Acres(jwind, kval) = Windows_Acres(jwind, kval) + Acres(jstand) ' windmultval * Acres(jstand)

            Next
        Next
        Lines = Nothing


        ReDim TempUniqueValues(NUniqueValues)
        For j As Integer = 1 To NUniqueValues
            TempUniqueValues(j) = Unique_Values(j)
        Next
        Array.Sort(TempUniqueValues)
        ReDim Unique_Values(NUniqueValues)
        Unique_Values = TempUniqueValues

        'also flag the min/max x and y for the window based on the included stands..
        FindMinMaxMapSize(begwind)

        CellWidth = DetermineCellWidth()

        FullMap.CellWidth = CellWidth
        DispMap.CellWidth = CellWidth

        DispMap.bDrawEdges = bDrawEdges
        DispMap.bDrawFireEdges = bDrawFireEvents
        bMapLoaded = True

    End Sub


    Private Sub LoadEVUSIMData(ByVal StandOutputFile As String, ByVal DispAttribute As String, ByVal WindowNumber As Integer)
        'in the future, prompt with a selector which fields contain info.

        Dim PerOutValue As Single = 0 'the output value for the stand in each period
        Dim k As Integer
        Dim StID As Integer = 0
        Dim MxStID As Integer = 0

        Dim kwind As Integer
        Dim kval As Integer
        Dim kcolval As Integer
        Dim kattcol As Integer 'index of the column for the attribute you are collecting
        Dim kstand As Integer
        Dim lasttime As Integer

        Dim CellWidth As Double

        Dim dummy As String
        Dim Atts() As String
        Dim TempUniqueValues() As Single

        Dim begtic As Integer = 0
        Dim begwind As Integer = 0
        Dim lastStandID As Integer

        NWindows = 0

        '-------------------------------------------
        'read windows file and fill info. about number of time periods and number of stands

        NWindows = 0
        Dim Lines = IO.File.ReadAllLines(StandOutputFile)

        'find the column index we are populating
        kattcol = 0
        Atts = Split(Lines(0), ",")
        For j As Integer = 0 To Atts.Length - 1
            If Atts(j) = DispAttribute Then
                kattcol = j
                Exit For
            End If
        Next

        'find the number of windows, number of stands, and define windows labels.
        Atts = Split(Lines(Lines.length - 1), ",")
        NWindows = Atts(1)
        ReDim Windows_Label(NWindows), Windows_Period(NWindows)
        For j As Integer = 0 To NWindows
            Windows_Label(j) = DispAttribute & "_" & j
            Windows_Period(j) = j
        Next
        MxStandID = Atts(3)

        ReDim Windows_MinX(NWindows), Windows_MinY(NWindows), Windows_MaxX(NWindows) 'StandIndByStandID(MxStID), 
        ReDim Windows_MaxY(NWindows), Windows_NWindStands(NWindows)
        ReDim Windows_Acres(NWindows, NColors) ', Stands(NStands)
        ReDim Windows_Value(NWindows, MxStandID) ', Windows_WindowColorInd(NWindows, NStands)

        For jwind As Integer = begwind To NWindows
            Windows_NWindStands(jwind) = MxStandID
        Next

        'read windows file again and fill period-by-period value
        ReDim iStandColor(NWindows, MxStandID)
        ReDim Unique_Values(MxStandID)
        NUniqueValues = 0
        Unique_Values(NUniqueValues) = 0

        lasttime = -1
        lastStandID = -1
        For jline As Integer = 1 To Lines.length - 1
            'If jstand = 26190 Then Stop
            dummy = Lines(jline) 'LineInput(fnum)
            Atts = Split(dummy, ",")
            StID = CType(Atts(3), Integer)
            'If StID = 119732 Then Stop
            If StID <> lastStandID Then
                lastStandID = StID

                If lasttime <> Atts(1) Then
                    kstand = 0 'start over
                    lasttime = Atts(1)
                End If

                kstand += 1

                kwind = lasttime
                'If PolyFileType = "GRID" Then kwind = 0
                PerOutValue = Math.Round(CType(Atts(kattcol), Single), 3)
                kcolval = GetSingleIndex(Unique_Values, PerOutValue)
                If kcolval < 0 Or NUniqueValues = 0 Then
                    NUniqueValues = NUniqueValues + 1
                    kcolval = NUniqueValues
                    Unique_Values(NUniqueValues) = PerOutValue
                End If
                Windows_Value(kwind, StID) = PerOutValue
                iStandColor(kwind, StID) = 0 'default

            End If
        Next
        Lines = Nothing

        ReDim TempUniqueValues(NUniqueValues)
        For j As Integer = 1 To NUniqueValues
            TempUniqueValues(j) = Unique_Values(j)
        Next
        Array.Sort(TempUniqueValues)
        ReDim Unique_Values(NUniqueValues)
        Unique_Values = TempUniqueValues

        'also flag the min/max x and y for the window based on the included stands..
        FindMinMaxMapSize(begwind)

        CellWidth = DetermineCellWidth()

        FullMap.CellWidth = CellWidth
        DispMap.CellWidth = CellWidth

        DispMap.bDrawEdges = bDrawEdges
        DispMap.bDrawFireEdges = bDrawFireEvents
        bMapLoaded = True

    End Sub

    Private Sub MakeEVUSimDataColorFile(ByVal LUTFile As String)
        Dim dummy As String
        Dim ind As Integer
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        Dim IDArr() As Integer
        Dim LabelArr() As String
        Dim arr() As String
        Dim kcolor As Integer



        NColors = NUniqueValues
        ReDim LegendColor(NColors)
        'ChangeColor = LegendColor(0)
        If System.IO.File.Exists(LUTFile) Then
            Dim Lines = IO.File.ReadAllLines(LUTFile)
            ReDim IDArr(Lines.length - 1), LabelArr(Lines.length - 1)
            For jline As Integer = 1 To Lines.length - 1
                arr = Split(Lines(jline), ",")
                IDArr(jline) = arr(0)
                LabelArr(jline) = arr(1)
            Next
        Else
            ReDim IDArr(NUniqueValues), LabelArr(NUniqueValues)
            For jcolor As Integer = 1 To NUniqueValues
                IDArr(jcolor) = Unique_Values(jcolor)
                LabelArr(jcolor) = Unique_Values(jcolor)
            Next
        End If
        For jcolor As Integer = 1 To NUniqueValues

            r = Rnd() * 255
            g = Rnd() * 255
            b = Rnd() * 255
            LegendColor(jcolor).Color = Color.FromArgb(255, r, g, b)
            LegendColor(jcolor).LabelColorInd = jcolor
            'Windows_Label(jcolor) = "INDEX"
            'find this index
            kcolor = -1
            For j As Integer = 1 To IDArr.Length - 1
                If IDArr(j) = Unique_Values(jcolor) Then
                    kcolor = j
                    Exit For
                End If
            Next
            If kcolor > -1 Then
                LegendColor(jcolor).LabelDispStr = LabelArr(kcolor) 'Unique_Values(jcolor)
                LegendColor(jcolor).LabelMinVal = IDArr(kcolor) 'Unique_Values(jcolor)
                LegendColor(jcolor).LabelMaxVal = IDArr(kcolor) 'Unique_Values(jcolor)
            Else
                LegendColor(jcolor).LabelDispStr = Unique_Values(jcolor)
                LegendColor(jcolor).LabelMinVal = Unique_Values(jcolor)
                LegendColor(jcolor).LabelMaxVal = Unique_Values(jcolor)
            End If


        Next
        'If bColorSaved = True Then
        'ind = NColors + 1
        'NColors = ind
        'ReDim Preserve LegendColor(ind)
        'LegendColor(ind) = ChangeColor
        'LegendColor(ind).LabelColorInd = NColors
        Dim fnum As Integer
        fnum = FreeFile()
        'read in the lines that are there now
        If System.IO.File.Exists(ColorIndexFile) = False Then

            bColorFile = True
        End If

        FileOpen(fnum, ColorIndexFile, OpenMode.Output)
        PrintLine(fnum, "NColors")
        PrintLine(fnum, NColors)
        PrintLine(fnum, "LabelColorIndex(i),ColmnName(s),LabelString(s),LabelMinVal(i),LabelMaxVal(i),R,G,B")
        For jline As Integer = 1 To NColors  'rtbColorFile.Lines.Length - 1
            dummy = LegendColor(jline).LabelColorInd & "," & Windows_Label(0) & "," & LegendColor(jline).LabelDispStr & "," & Math.Round(LegendColor(jline).LabelMinVal, 2) & "," & Math.Round(LegendColor(jline).LabelMaxVal, 2)
            dummy = dummy & "," & LegendColor(jline).Color.R & "," & LegendColor(jline).Color.G & "," & LegendColor(jline).Color.B
            PrintLine(fnum, dummy)
        Next
        FileClose(fnum)

        LoadWindowColorInfo(ColorIndexFile)
        ReDefineMapColors()
        ListView1.SelectedItems.Clear()
 
    End Sub

    Private Sub LoadStaticMetricsFile(ByVal Filename As String)

        'should load either a .attributesall file or a SLINKMETRICS file

        Dim dummy As String
        Dim dummy2 As String
        Dim arr() As String
        Dim RowInd As Integer
        Dim ColInd As Integer
        Dim PolyIDInd As Integer

        Dim AcreInd As Integer = -1
        Dim bAttsAllFile As Boolean

        Dim row As Integer
        Dim col As Integer
        Dim PolyID As Integer
        Dim NCells As Integer

        Dim Lines = IO.File.ReadAllLines(Filename)

        'special case if you are reading a .attributesall file
        bAttsAllFile = False
        If Filename.Length >= 14 Then
            If Filename.Substring(Filename.Length - 13, 13) = "attributesall" Then bAttsAllFile = True
        End If
        'the header line - find the appropriate indices
        dummy = Lines(0) 'Trim(LineInput(fnum))
        If bAttsAllFile = True Then
            NumAtts = 27
            ReDim AttName(NumAtts)
            For jatt As Integer = 1 To NumAtts
                AttName(jatt) = "#" 'default
            Next
            PolyIDInd = 0
            AttName(1) = "SLINK"
            ColInd = 2
            AttName(2) = "ROW"
            RowInd = 1
            AttName(3) = "COL"
            arr = Split(dummy, ",")
            AcreInd = 4

            AttName(5) = "ACRES"
            AttName(6) = "HabGroup"
            AttName(9) = "Species"
            AttName(10) = "SizeClass"
            AttName(11) = "Density"
            AttName(12) = "Process"
            AttName(18) = "Ownership"
            AttName(21) = "FMZ"
            AttName(22) = "SpecialArea"
        ElseIf bAttsAllFile = False Then 'assume you have a meaningful header
            arr = Split(dummy, ",")
            NumAtts = arr.Length + 1 'the last is reserved for the current value of the window
            ReDim AttName(NumAtts)
            For j As Integer = 0 To arr.Length - 1
                dummy2 = StripQuotes(arr(j))
                'FOR SOME REASON THE ROW/COLUMN INDICES SEEM TO BE SWITCHED
                If dummy2 = "ROW" Or dummy2 = "Row" Or dummy2 = "row" Then RowInd = j
                If dummy2 = "COL" Or dummy2 = "Col" Or dummy2 = "col" Or dummy2 = "COLUMN" Then ColInd = j
                If dummy2 = "SLINK" Or dummy2 = "Slink" Or dummy2 = "slink" Then PolyIDInd = j
                If dummy2 = "Acres" Or dummy2 = "ACRES" Or dummy2 = "acres" Then
                    AcreInd = j
                End If
                AttName(j + 1) = arr(j)
            Next
         
        End If

        AttName(NumAtts - 1) = "CurrentValue"
        AttName(NumAtts) = "Area"
        tsbcbbStandPoly.Items.Clear()
        For jatt As Integer = 1 To NumAtts
            tsbcbbStandPoly.Items.Add(AttName(jatt))
        Next
        tsbcbbStandPoly.Items.Add("<no display>")

        'find biggest row,col and count the polygons
        BiggestRow = -99
        BiggestCol = -99
        MxStandID = 0

        For j As Integer = 1 To Lines.length - 1 'STARTING AT 1 skips the header
            dummy = Trim(Lines(j)) 'Trim(LineInput(fnum))
            If dummy.Length = 0 Or Trim(dummy) = "END" Then Exit For 'do
            NCells = NCells + 1
            arr = Split(dummy, ",")
            If arr(RowInd) > BiggestRow Then BiggestRow = arr(RowInd)
            If arr(ColInd) > BiggestCol Then BiggestCol = arr(ColInd)
            If arr(PolyIDInd) > MxStandID Then MxStandID = arr(PolyIDInd)
        Next j
        If NCells < MxStandID Then NCells = MxStandID

        ReDim PolyAtts(MxStandID, NumAtts)


        'populate the arrays
        ReDim GridSlink(BiggestRow, BiggestCol)
        ReDim GRIDPolys_PolyID(MxStandID), GRIDPolys_xVal(MxStandID), GRIDPolys_yVal(MxStandID) ', GRIDPolys_StandID(MxStandID), GRIDPolys_ColorInd(Ncells), GRIDPolys_col(Ncells), GRIDPolys_row(Ncells), _
        ReDim Acres(MxStandID)
        ReDim PolyIndByPolyID(MxStandID)

        For j As Integer = 1 To Lines.length - 1
            dummy = Trim(Lines(j)) 'Trim(LineInput(fnum))
            If dummy.Length = 0 Or Trim(dummy) = "END" Then Exit For 'Do
            arr = Split(dummy, ",")

            row = arr(RowInd)
            col = arr(ColInd)
            PolyID = arr(PolyIDInd)
            GridSlink(row, col) = PolyID

            For jatt As Integer = 1 To NumAtts - 2 'last two are filled with the identify and the total area
                PolyAtts(PolyID, jatt) = arr(jatt - 1)
            Next

            If AcreInd >= 0 Then Acres(PolyID) = arr(AcreInd)
            GRIDPolys_PolyID(PolyID) = arr(PolyIDInd)

            GRIDPolys_xVal(PolyID) = col 'arr(ColInd) 'arr(XField)
            GRIDPolys_yVal(PolyID) = row 'arr(RowInd) 'arr(YField)

            'fill the standidbypolyid array to get stand id when you input polyid
            PolyIndByPolyID(GRIDPolys_PolyID(PolyID)) = PolyID
        Next j
        Lines = Nothing

        'and define the adjacencies
        PopulateAdjacencies()

    End Sub


    Private Sub FindMinMaxMapSize(ByVal begwind As Integer)
        Dim WindowStandCount() As Integer
        Dim PolyID As Integer
        Dim gMinX As Long
        Dim gMinY As Long
        Dim gMaxX As Long
        Dim gMaxY As Long

        'set the number of stands in each window...
        ReDim WindowStandCount(NWindows)
        For jwindow As Integer = 0 To NWindows
            WindowStandCount(jwindow) = 0
            Windows_MinX(jwindow) = 9999999999
            Windows_MinY(jwindow) = 9999999999
            Windows_MaxX(jwindow) = -9999999999
            Windows_MaxY(jwindow) = -9999999999
        Next
        gMinX = 9999999999
        gMinY = 9999999999
        gMaxX = -9999999999
        gMaxY = -9999999999

        For jcell As Integer = 1 To MxStandID
            PolyID = GRIDPolys_PolyID(jcell)
            For jwindow As Integer = 0 To NWindows
                WindowStandCount(jwindow) += 1

                Windows_MinX(jwindow) = Math.Min(Windows_MinX(jwindow), GRIDPolys_xVal(PolyID))
                Windows_MaxX(jwindow) = Math.Max(Windows_MaxX(jwindow), GRIDPolys_xVal(PolyID))
                Windows_MinY(jwindow) = Math.Min(Windows_MinY(jwindow), GRIDPolys_yVal(PolyID))
                Windows_MaxY(jwindow) = Math.Max(Windows_MaxY(jwindow), GRIDPolys_yVal(PolyID))

            Next

            'find the global min/max x/y values

            gMinX = Math.Min(gMinX, GRIDPolys_xVal(PolyID))
            gMaxX = Math.Max(gMaxX, GRIDPolys_xVal(PolyID)) 'GRIDPolys_xVal(PolyID)
            gMinY = Math.Min(gMinY, GRIDPolys_yVal(PolyID)) 'GRIDPolys_yVal(PolyID)
            gMaxY = Math.Max(gMaxY, GRIDPolys_yVal(PolyID))

        Next

        FullMap.MaxX = gMaxX
        FullMap.MaxY = gMaxY
        FullMap.MinX = gMinX
        FullMap.MinY = gMinY
  
        DispMap.MaxX = Windows_MaxX(begwind)
        DispMap.MinX = Windows_MinX(begwind)
        DispMap.MaxY = Windows_MaxY(begwind)
        DispMap.MinY = Windows_MinY(begwind)
 
    End Sub

    Private Sub PopulateAdjacencies()
        'fills in GRIDPolys_AdjCellInd
        'assumes GridSlink(row,col) is filled
        Dim row As Integer
        Dim col As Integer

        'populate adjacencies - 8 way
        ReDim GRIDPolys_AdjCellInd(MxStandID, 7)
        For jcell As Integer = 1 To MxStandID
            row = GRIDPolys_yVal(jcell)
            col = GRIDPolys_xVal(jcell)

            'Poly indexes of adjacent polygons...
            'move from west polygon counter-clockwise around
            If col - 1 > 0 Then GRIDPolys_AdjCellInd(jcell, 0) = GridSlink(row, col - 1) 'west
            If col - 1 > 0 And row - 1 > 0 Then GRIDPolys_AdjCellInd(jcell, 1) = GridSlink(row - 1, col - 1) 'sw
            If row - 1 > 0 Then GRIDPolys_AdjCellInd(jcell, 2) = GridSlink(row - 1, col) 'south
            If row - 1 > 0 And col + 1 <= BiggestCol Then GRIDPolys_AdjCellInd(jcell, 3) = GridSlink(row - 1, col + 1) 'se
            If col + 1 <= BiggestCol Then GRIDPolys_AdjCellInd(jcell, 4) = GridSlink(row, col + 1) 'east
            If row + 1 <= BiggestRow And col + 1 <= BiggestCol Then GRIDPolys_AdjCellInd(jcell, 5) = GridSlink(row + 1, col + 1) 'ne
            If row + 1 <= BiggestRow Then GRIDPolys_AdjCellInd(jcell, 6) = GridSlink(row + 1, col) 'north
            If row + 1 <= BiggestRow And col - 1 > 0 Then GRIDPolys_AdjCellInd(jcell, 7) = GridSlink(row + 1, col - 1) 'nw

        Next jcell


    End Sub

    Private Sub FillPolyDispValue(ByVal ColInd As Integer)
        'sub fills the GRIDPolys_StandID value for each cell for display, query, and identity purposes
        Dim tval As String

        'ReDim StandValues(MxStandID)
        ReDim GRIDPolys_DisplayID(MxStandID)
        NStandValues = 0

        For jcell As Integer = 1 To MxStandID
            tval = PolyAtts(jcell, ColInd)

            GRIDPolys_DisplayID(jcell) = tval 'arr(StandIDInd)
        Next jcell
        FillEdgeInfo()
        bDrawEdges = cbDrawEdge.Checked
        bDrawNewEdges = bDrawEdges * 1
        If bMapLoaded = True Then MoveWindow(CurrWindInd)
    End Sub
    Private Sub FillPolyIdentifyValue(ByVal ColInd As Integer)
        'sub fills the GRIDPolys_StandID value for each cell for display, query, and identity purposes
        Dim tval As String

        'ReDim StandValues(MxStandID)
        ReDim GRIDPolys_IdentifyID(MxStandID)
        NStandValues = 0

        For jcell As Integer = 1 To MxStandID
            tval = PolyAtts(jcell, ColInd)

            GRIDPolys_IdentifyID(jcell) = tval 'arr(StandIDInd)
        Next jcell
        FillEdgeInfo()
        bDrawEdges = cbDrawEdge.Checked
        bDrawNewEdges = bDrawEdges * 1
        'If bMapLoaded = True Then MoveWindow(CurrWindInd)
    End Sub
    Private Sub FillPolyQueryValue(ByVal ColInd As Integer)
        'sub fills the GRIDPolys_StandID value for each cell for display, query, and identity purposes
        Dim tval As String

        'ReDim StandValues(MxStandID)
        ReDim GRIDPolys_QueryID(MxStandID)
        NStandValues = 0

        For jcell As Integer = 1 To MxStandID
            tval = PolyAtts(jcell, ColInd)

            GRIDPolys_QueryID(jcell) = tval 'arr(StandIDInd)
        Next jcell
        FillEdgeInfo()
        bDrawEdges = cbDrawEdge.Checked
        bDrawNewEdges = bDrawEdges * 1
        'If bMapLoaded = True Then MoveWindow(CurrWindInd)
    End Sub

    Private Sub LoadFireEvents(ByVal FireEventFN As String)
        Dim fnum As Integer
        Dim dummy As String
        Dim arr() As String
        Dim kslink As Integer
        ReDim FireEvent(MxStandID, NWindows)
        fnum = FreeFile()
        FileOpen(fnum, FireEventFN, OpenMode.Input)
        dummy = LineInput(fnum)
        Do Until EOF(fnum)
            dummy = Trim(LineInput(fnum))
            If dummy.Length = 0 Then Exit Do
            arr = Split(dummy, ",")
            kslink = arr(0)
            For j As Integer = 0 To NWindows
                FireEvent(kslink, j) = arr(j + 1)
            Next
        Loop

        FileClose(fnum)
    End Sub
    Private Sub ConvertFireEventFile(ByVal FireEventFN As String, ByRef NewFireEventFN As String)
        Dim fnum As Integer
        Dim ofnum As Integer
        Dim dummy As String
        Dim sdummy As System.Text.StringBuilder
        Dim per As Integer
        Dim slink As Integer
        Dim slink2 As Integer
        Dim SLINKFireEventByPer(,) As Integer 'holds index of originating fire - dimmed by nslink,npers
        Dim arr() As String
        Dim arr2() As String
        Dim arr3() As String
        Dim ind As Integer
        Dim kwind As Integer 'the period associated with the window attributes you are populating.Each window has an associated period. If a period is not represented in a window, then skip that period

        ReDim SLINKFireEventByPer(MxStandID, NWindows)

        fnum = FreeFile()
        FileOpen(fnum, FireEventFN, OpenMode.Input)

        Do Until EOF(fnum) 'first read the number of periods, number of SLINKs
            dummy = LineInput(fnum)
            If Trim(dummy) = "Fire Event Summary" Then Exit Do
            arr = Split(dummy, " ")
            ind = -1
            ReDim arr3(arr.Length - 1)
            For j As Integer = 0 To arr.Length - 1
                If arr(j) <> "" Then
                    ind = ind + 1
                    arr3(ind) = arr(j)
                End If
            Next
            If ind > 1 Then
                If arr3(2) = "MSF" Or arr3(2) = "LSF" Or arr3(2) = "SRF" Then
                    slink = arr3(1)
                    per = arr3(0)

                    If per > NWindows Then Exit Do 'you're not using all time periods in the file and have not dimmed slinkfireeenbyper big enough to handle
                    kwind = Array.IndexOf(Windows_Period, per)
                    If kwind >= 0 Then 'period is included in a window
                        For j As Integer = 9 To arr3.Length - 1
                            If arr3(j) <> "" Then
                                arr2 = Split(arr3(j), "-")
                                slink2 = arr2(0)
                                SLINKFireEventByPer(slink2, kwind) = slink 'index the fire by the origin slink
                            End If
                        Next
                    End If
                End If
            End If
        Loop
        FileClose(fnum)

        ofnum = FreeFile()
        NewFireEventFN = System.IO.Path.GetDirectoryName(FireEventFN) & "\" & FName(FireEventFN) & "_MMaPPit.txt"
        FileOpen(ofnum, NewFireEventFN, OpenMode.Output)
        sdummy = New System.Text.StringBuilder("SLINK", 300)
        For jper As Integer = 0 To NWindows
            sdummy.Append("," & Windows_Period(jper))
        Next
        PrintLine(ofnum, sdummy.ToString)
        sdummy = Nothing

        For jslink As Integer = 1 To MxStandID
            sdummy = New System.Text.StringBuilder(jslink.ToString, 300)
            For jper As Integer = 0 To NWindows
                sdummy.Append("," & SLINKFireEventByPer(jslink, jper))
            Next
            PrintLine(ofnum, sdummy.ToString)
            sdummy = Nothing
        Next
        FileClose(ofnum)

        SLINKFireEventByPer = Nothing
    End Sub

    Private Sub ZoomBitmap(ByVal ZoomMap As MapInfo, ByVal DisplayMap As MapInfo, ByVal Colors() As ColorInfo, ByVal DrawEdge As Boolean, ByVal DrawFireEdge As Boolean, ByVal bImageMove As Boolean) ', ByVal WindInd As Integer)
        'ZoomMap is the coordinates of the window to zoom to
        'DisplayMap is the currently displayed map properties

        Dim g As System.Drawing.Graphics
        Dim g2 As System.Drawing.Graphics
        'Dim gfi As System.Drawing.Graphics
        Dim CellWidth As Double
        'Dim AngleBetweenCenters As Double
        Dim FormWidth As Integer
        Dim FormHeight As Integer
        Dim FormWHRatio As Double
        Dim MapWidth As Double
        Dim MapHeight As Double
        Dim InputWHRatio As Double
        Dim WidthToAdd As Integer
        Dim HeightToAdd As Integer
        'Dim HexHeight As Double 'distance from top point to bottom point
        Dim AdjFactor As Double '
        Dim xScale As Double 'selection to zoom has to be modified because proportionally the map to display is narrower than the form
        Dim yScale As Double 'if your selection is of
        Dim NewBitMap As System.Drawing.Bitmap
        Dim NewBitMap2 As System.Drawing.Bitmap

        Dim drawXDist As Double 'distance from center point to draw in the x direction (adjusted)
        Dim drawYDist As Double 'distance from center point to draw in the y direction (adjusted)

        Dim jpoly As Integer
        Dim j As Integer
        Dim p1 As Integer 'point of line to draw
        Dim p2 As Integer 'point 2 of line to draw
        Dim fPen As New Pen(FireEventColor, 3) 'for fire boundaries
        Dim oPen As New Pen(FireOriginColor, 3)

        Dim TransX As Double 'translated center polygon
        Dim TransY As Double '

        Dim YSlack As Integer 'extra space in y dimension

        Dim ZoomMapDisp As MapInfo 'information about the portion of the map to zoom to...

        'what is the size of the original display?
        FormWidth = panelMap.Size.Width
        FormHeight = panelMap.Size.Height
        FormWHRatio = FormWidth / FormHeight

        'what is the size of the selected zoom portion?
        MapWidth = ZoomMap.MaxX - ZoomMap.MinX
        MapHeight = ZoomMap.MaxY - ZoomMap.MinY

        'HOW SHOULD THE ZOOM PORTION MAP THE ORIGINAL?
        InputWHRatio = (ZoomMap.MaxX - ZoomMap.MinX) / (ZoomMap.MaxY - ZoomMap.MinY)
        If InputWHRatio < FormWHRatio Then
            'selection is wider than form - have to add to width
            HeightToAdd = 0
            WidthToAdd = (FormWidth * MapHeight / FormHeight) - MapWidth
        Else
            WidthToAdd = 0
            HeightToAdd = (FormHeight * MapWidth / FormWidth) - MapHeight
        End If
        'now adjust parameters
        ZoomMap.MinX = ZoomMap.MinX - WidthToAdd / 2
        ZoomMap.MaxX = ZoomMap.MaxX + WidthToAdd / 2
        ZoomMap.MinY = ZoomMap.MinY - HeightToAdd / 2
        ZoomMap.MaxY = ZoomMap.MaxY + HeightToAdd / 2

        'NOW FIND OUT WHETHER THE HEIGHT OR THE WIDTH OF THE DISPLAY MAP HAS TO BE MODIFIED
        MapWidth = DisplayMap.MaxX - DisplayMap.MinX
        MapHeight = DisplayMap.MaxY - DisplayMap.MinY
        If MapWidth <= FormWidth And MapHeight <= FormHeight Then
            'need to scale up!
            AdjFactor = Math.Min(FormWidth / MapWidth, FormHeight / MapHeight)
            'map height is too big
        ElseIf MapHeight >= FormHeight And MapWidth <= FormWidth Then
            AdjFactor = FormHeight / MapHeight
            'map length is too big
        ElseIf MapHeight <= FormHeight And MapWidth >= FormWidth Then
            AdjFactor = FormWidth / MapWidth
        Else
            'both mapheight and mapwidth are too big
            AdjFactor = Math.Min(FormWidth / MapWidth, FormHeight / MapHeight)
        End If
        'now find xscale and yscale..
        xScale = AdjFactor * MapWidth / FormWidth
        yScale = AdjFactor * MapHeight / FormHeight

        'what are the relative min/max coordinates of the original map to be displayed?
        'relate them to the dispmap
        'ratio of new width to old * width of the display + min x parameter of the display...
        'needs to be adjusted to account for different shapes between the dispmap and the form...

        ZoomMapDisp.MinX = ((ZoomMap.MinX / FormWidth) * MapWidth) / xScale + DisplayMap.MinX
        ZoomMapDisp.MaxX = ((ZoomMap.MaxX / FormWidth) * MapWidth) / xScale + DisplayMap.MinX
        ZoomMapDisp.MaxY = (DisplayMap.MaxY - ((ZoomMap.MinY / FormHeight) * (MapHeight) / yScale))
        ZoomMapDisp.MinY = (DisplayMap.MaxY - ((ZoomMap.MaxY / FormHeight) * (MapHeight) / yScale))
        ZoomMapDisp.xScale = xScale
        ZoomMapDisp.yScale = yScale

        'only do this if you are not panning or doing fixed zooms
        If bImageMove = False Then 'If tsbPan.Checked = False Then
            ZoomMapDisp.MinX = Math.Max(ZoomMapDisp.MinX, DisplayMap.MinX)
            ZoomMapDisp.MinY = Math.Max(ZoomMapDisp.MinY, DisplayMap.MinY)

            ZoomMapDisp.MaxX = Math.Min(ZoomMapDisp.MaxX, DisplayMap.MaxX)
            ZoomMapDisp.MaxY = Math.Min(ZoomMapDisp.MaxY, DisplayMap.MaxY)
        End If

        ZoomMapDisp.CellWidth = DisplayMap.CellWidth


        DisplayMap = ZoomMapDisp
        'show the display map x/y values in the text box
        tbCoords.Text = Math.Round(DisplayMap.MinX, 0) & "," & Math.Round(DisplayMap.MaxX, 0) & "," & Math.Round(DisplayMap.MinY, 0) & "," & Math.Round(DisplayMap.MaxY, 0)

        CellWidth = DisplayMap.CellWidth

        MapWidth = (DisplayMap.MaxX - DisplayMap.MinX) + CellWidth 'have to pad for 1/2 hex on either side
        MapHeight = (DisplayMap.MaxY - DisplayMap.MinY) + CellWidth

        'logic: if map completely fits in form then have to scale map up
        '       if map is bigger than form in any direction, have to scale map down

        If MapWidth <= FormWidth And MapHeight <= FormHeight Then
            'need to scale up!
            AdjFactor = Math.Min(FormWidth / MapWidth, FormHeight / MapHeight)
            'map height is too big
        ElseIf MapHeight >= FormHeight And MapWidth <= FormWidth Then
            AdjFactor = FormHeight / MapHeight
            'map length is too big
        ElseIf MapHeight <= FormHeight And MapWidth >= FormWidth Then
            AdjFactor = FormWidth / MapWidth
        Else
            'both mapheight and mapwidth are too big
            AdjFactor = Math.Min(FormWidth / MapWidth, FormHeight / MapHeight)
        End If

        drawXDist = AdjFactor * CellWidth / 2 * Math.Sqrt(2)
        drawYDist = AdjFactor * CellWidth / 2 * Math.Sqrt(2)


        YSlack = FormHeight - MapHeight * AdjFactor 'keeps map at the top

        Dim points() As Drawing.PointF
        Dim DegAsRad45 As Double 'thirty degrees expressed in radians...
        DegAsRad45 = Math.PI / 4

        NewBitMap = New Bitmap(FormWidth, FormHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
        g = Graphics.FromImage(NewBitMap)
        NewBitMap2 = New Bitmap(FormWidth, FormHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
        g2 = Graphics.FromImage(NewBitMap2)
        'FPBitmap = New Bitmap(Me.Width, Me.Height)
        'gfi = Graphics.FromImage(FPBitmap)

        If DrawFireEdge Then FillFireEdgeInfo(CurrWindInd)
        'If DrawEdge Then FillEdgeInfo()

        For jpoly = 1 To MxStandID
            ReDim points(3)
            TransX = drawXDist + (GRIDPolys_xVal(jpoly) - DisplayMap.MinX) * AdjFactor
            TransY = FormHeight - YSlack - drawYDist - (GRIDPolys_yVal(jpoly) - DisplayMap.MinY) * AdjFactor

            'if center is within the map...
            If TransX >= 0 - drawXDist And TransX <= FormWidth + drawXDist And TransY >= 0 - drawYDist And TransY <= FormHeight + drawYDist Then
                '######### cardinal direction labeling may be flawed due to reverse y direction...needs checking
                'Populate points with HexHeight and starting with the Angle between centers as a reference...
                'Have to compensate for the angle is calculated in Quadrant 1, and when plotted, the angle 
                '  interpreted as being in Quadrant 4

                ' northeast - first point is the angle between centers + 45 degrees...
                points(1).X = Math.Round(TransX + Math.Cos(DegAsRad45) * drawYDist, 3) 'x is fine
                points(1).Y = Math.Round(TransY - Math.Sin(DegAsRad45) * drawYDist, 3)
                'northwest
                points(0).X = Math.Round(TransX + Math.Cos(3 * DegAsRad45) * drawYDist, 3)
                points(0).Y = Math.Round(TransY - Math.Sin(3 * DegAsRad45) * drawYDist, 3)
                'southwest
                points(3).X = Math.Round(TransX + Math.Cos(5 * DegAsRad45) * drawYDist, 3)
                points(3).Y = Math.Round(TransY - Math.Sin(5 * DegAsRad45) * drawYDist, 3)
                'southeast
                points(2).X = Math.Round(TransX + Math.Cos(7 * DegAsRad45) * drawYDist, 3)
                points(2).Y = Math.Round(TransY - Math.Sin(7 * DegAsRad45) * drawYDist, 3)

                'g.FillPolygon(New SolidBrush(Colors(GRIDPolys_ColorInd(jpoly)).Color), points)
                g.FillPolygon(New SolidBrush(Colors(iStandColor(CurrWindInd, jpoly)).Color), points)

                ' THE FOLLOWING DRAW EDGE IS GOOD CODE, JUST NOT RESOLVED AFTER THE ANGLE POINT DRAWING METHOD...
                'DO NOT DELETE!!!
                If DrawEdge Then 'Assumption...these are indexed starting W counterclockwise
                    For j = 0 To 7
                        If j = 0 Then
                            p1 = 0 'western edge
                            p2 = 3
                        ElseIf j = 2 Then
                            p1 = 3 'nw
                            p2 = 2 'ne
                        ElseIf j = 4 Then
                            p1 = 2 'ne
                            p2 = 1 'se
                        ElseIf j = 6 Then
                            p1 = 1 'se
                            p2 = 0 'sw
                        End If

                        If GRIDPolys_Edge(jpoly, j) = 1 And (j = 0 Or j = 2 Or j = 4 Or j = 6) Then
                            g2.DrawLine(Pens.Black, points(p1), points(p2))
                        End If
                    Next
                End If
                If DrawFireEdge = True Then
                    For j = 1 To 4
                        If j = 1 Then 'western edge
                            'If FireEventEdgeQual = 1 Then
                            p1 = 0 'sw
                            p2 = 3 'ne

                        ElseIf j = 2 Then 'northern edge
                            'If FireEventEdgeQual = 1 Then
                            p1 = 3 'nw
                            p2 = 2 'ne

                        ElseIf j = 3 Then
                            'If FireEventEdgeQual = 1 Then
                            p1 = 2 'ne
                            p2 = 1 'se

                        ElseIf j = 4 Then 'southern edge
                            'If FireEventEdgeQual = 1 Then
                            p1 = 1 'se
                            p2 = 0 'sw

                        End If
                        'If FireOrigin(jpoly) = 1 Then Stop
                        If FireEventEdge(jpoly, j) = 1 And FireOrigin(jpoly) = 0 Then 'And (j = 0 Or j = 2 Or j = 4 Or j = 6) Then
                            g.DrawLine(fPen, points(p1), points(p2))
                        ElseIf FireOrigin(jpoly) = True Then
                            g.DrawLine(oPen, points(p1), points(p2))
                        End If
                    Next
                End If
            End If
        Next


        PaintBitMap = NewBitMap
        DisplayEdgeBitMap = NewBitMap2


        If bWindStage = True Then
            WindStageBitMap(CurrWindInd) = NewBitMap
            bPainted(CurrWindInd) = True
            'FullProgramBitMap(CurrWindInd) = FPBitmap
        End If

        DispMap = DisplayMap
        g.Dispose()
        g2.Dispose()
        fPen = Nothing
        panelMap.Invalidate()


    End Sub

    Private Sub DetermineIdentifyValue(ByVal DisplayMap As MapInfo, ByVal XCoord As Double, ByVal YCoord As Double, ByRef StandIDToDraw As String, ByRef polyID As Integer)
        Dim TransX As Double 'translated center polygon
        Dim TransY As Double '
        Dim MapWidth As Double
        Dim MapHeight As Double
        Dim FormWidth As Integer
        Dim FormHeight As Integer
        Dim FormWHRatio As Double
        Dim CellWidth As Double
        Dim AdjFactor As Double '
        Dim drawXDist As Double 'distance from center point to draw in the x direction (adjusted)
        Dim drawYDist As Double 'distance from center point to draw in the y direction (adjusted)
        Dim YSlack As Integer 'extra space in y dimension

        'what is the size of the original display?
        FormWidth = panelMap.Size.Width
        FormHeight = panelMap.Size.Height
        FormWHRatio = FormWidth / FormHeight

        CellWidth = DisplayMap.CellWidth

        MapWidth = (DisplayMap.MaxX - DisplayMap.MinX) + CellWidth 'have to pad for 1/2 hex on either side
        MapHeight = (DisplayMap.MaxY - DisplayMap.MinY) + CellWidth

        'logic: if map completely fits in form then have to scale map up
        '       if map is bigger than form in any direction, have to scale map down

        If MapWidth <= FormWidth And MapHeight <= FormHeight Then
            'need to scale up!
            AdjFactor = Math.Min(FormWidth / MapWidth, FormHeight / MapHeight)
            'map height is too big
        ElseIf MapHeight >= FormHeight And MapWidth <= FormWidth Then
            AdjFactor = FormHeight / MapHeight
            'map length is too big
        ElseIf MapHeight <= FormHeight And MapWidth >= FormWidth Then
            AdjFactor = FormWidth / MapWidth
        Else
            'both mapheight and mapwidth are too big
            AdjFactor = Math.Min(FormWidth / MapWidth, FormHeight / MapHeight)
        End If

        drawXDist = AdjFactor * CellWidth / 2 * Math.Sqrt(2)
        drawYDist = AdjFactor * CellWidth / 2 * Math.Sqrt(2)

        YSlack = FormHeight - MapHeight * AdjFactor 'keeps map at the top


        Dim DegAsRad45 As Double 'thirty degrees expressed in radians...
        DegAsRad45 = Math.PI / 4

        Dim SearchRad As Double = (drawXDist + drawYDist) / 2

        'FIND WHICH STAND THE SELECTED POLY IS IN
        For jpoly As Integer = 1 To MxStandID
            TransX = drawXDist + (GRIDPolys_xVal(jpoly) - DisplayMap.MinX) * AdjFactor
            TransY = FormHeight - YSlack - drawYDist - (GRIDPolys_yVal(jpoly) - DisplayMap.MinY) * AdjFactor

            'if center is close to the clicked point
            If TransX >= XCoord - SearchRad And TransX <= XCoord + SearchRad And TransY >= YCoord - SearchRad And TransY <= YCoord + SearchRad Then
                StandIDToDraw = GRIDPolys_IdentifyID(jpoly)
                polyID = jpoly
                Exit For
            End If
        Next jpoly
    End Sub
    Private Sub IdentifyMapSpace(ByVal DisplayMap As MapInfo, ByVal StandIDToDraw As String, ByRef IDArea As Double) ' ByVal XCoord As Double, ByVal YCoord As Double, ByRef jpolyreturn As Integer, ByRef IDArea As Double) ', ByVal WindInd As Integer)
        'ZoomMap is the coordinates of the window to zoom to
        'DisplayMap is the currently displayed map properties
        Dim NewBitMap As System.Drawing.Bitmap
        Dim g As System.Drawing.Graphics
        Dim CellWidth As Double
        'Dim AngleBetweenCenters As Double
        Dim FormWidth As Integer
        Dim FormHeight As Integer
        'Dim FormWHRatio As Double
        Dim MapWidth As Double
        Dim MapHeight As Double
        'Dim HexHeight As Double 'distance from top point to bottom point
        Dim AdjFactor As Double '
        Dim drawXDist As Double 'distance from center point to draw in the x direction (adjusted)
        Dim drawYDist As Double 'distance from center point to draw in the y direction (adjusted)
        Dim jpoly As Integer
        Dim TransX As Double 'translated center polygon
        Dim TransY As Double '

        Dim YSlack As Integer 'extra space in y dimension

        'what is the size of the original display?
        FormWidth = panelMap.Size.Width
        FormHeight = panelMap.Size.Height
        'FormWHRatio = FormWidth / FormHeight


        CellWidth = DisplayMap.CellWidth
        MapWidth = (DisplayMap.MaxX - DisplayMap.MinX) + CellWidth 'have to pad for 1/2 hex on either side
        MapHeight = (DisplayMap.MaxY - DisplayMap.MinY) + CellWidth

        'logic: if map completely fits in form then have to scale map up
        '       if map is bigger than form in any direction, have to scale map down

        If MapWidth <= FormWidth And MapHeight <= FormHeight Then
            'need to scale up!
            AdjFactor = Math.Min(FormWidth / MapWidth, FormHeight / MapHeight)
            'map height is too big
        ElseIf MapHeight >= FormHeight And MapWidth <= FormWidth Then
            AdjFactor = FormHeight / MapHeight
            'map length is too big
        ElseIf MapHeight <= FormHeight And MapWidth >= FormWidth Then
            AdjFactor = FormWidth / MapWidth
        Else
            'both mapheight and mapwidth are too big
            AdjFactor = Math.Min(FormWidth / MapWidth, FormHeight / MapHeight)
        End If

        drawXDist = AdjFactor * CellWidth / 2 * Math.Sqrt(2)
        drawYDist = AdjFactor * CellWidth / 2 * Math.Sqrt(2)
        YSlack = FormHeight - MapHeight * AdjFactor 'keeps map at the top

        Dim points() As Drawing.PointF

        Dim DegAsRad45 As Double 'thirty degrees expressed in radians...
        DegAsRad45 = Math.PI / 4


        NewBitMap = New Bitmap(FormWidth, FormHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
        g = Graphics.FromImage(NewBitMap)
        'FILL THE REST OF THE POLYS IN THE STAND

        IDArea = 0
        For jpoly = 1 To MxStandID
            If GRIDPolys_IdentifyID(jpoly) = StandIDToDraw Then
                TransX = drawXDist + (GRIDPolys_xVal(jpoly) - DisplayMap.MinX) * AdjFactor
                TransY = FormHeight - YSlack - drawYDist - (GRIDPolys_yVal(jpoly) - DisplayMap.MinY) * AdjFactor
                ReDim points(3)
                ' northeast - first point is the angle between centers + 45 degrees...
                points(1).X = Math.Round(TransX + Math.Cos(DegAsRad45) * drawYDist, 3) 'x is fine
                points(1).Y = Math.Round(TransY - Math.Sin(DegAsRad45) * drawYDist, 3)
                'northwest
                points(0).X = Math.Round(TransX + Math.Cos(3 * DegAsRad45) * drawYDist, 3)
                points(0).Y = Math.Round(TransY - Math.Sin(3 * DegAsRad45) * drawYDist, 3)
                'southwest
                points(3).X = Math.Round(TransX + Math.Cos(5 * DegAsRad45) * drawYDist, 3)
                points(3).Y = Math.Round(TransY - Math.Sin(5 * DegAsRad45) * drawYDist, 3)
                'southeast
                points(2).X = Math.Round(TransX + Math.Cos(7 * DegAsRad45) * drawYDist, 3)
                points(2).Y = Math.Round(TransY - Math.Sin(7 * DegAsRad45) * drawYDist, 3)

                g.FillPolygon(New SolidBrush(Color.FromArgb(IdentifyOpacity, 218, 165, 32)), points) '(Goldenrod), points)
                IDArea += Acres(jpoly)
            End If
        Next

        IDBitMap = NewBitMap
        DispMap = DisplayMap
        g.Dispose()
        panelMap.Invalidate()

    End Sub

    Private Sub QueryMapSpace(ByVal DisplayMap As MapInfo, ByVal StandIDToDraw() As String, ByRef SelectedArea As Double) ', ByRef jpolyreturn As Integer) ', ByVal WindInd As Integer)
        'ZoomMap is the coordinates of the window to zoom to

        Dim NewBitMap As System.Drawing.Bitmap
        Dim QueryThisPoly As Boolean
        Dim g As System.Drawing.Graphics
        Dim CellWidth As Double
        'Dim AngleBetweenCenters As Double
        Dim FormWidth As Integer
        Dim FormHeight As Integer
        Dim FormWHRatio As Double
        Dim MapWidth As Double
        Dim MapHeight As Double
        'Dim HexHeight As Double 'distance from top point to bottom point
        Dim AdjFactor As Double '
        Dim drawXDist As Double 'distance from center point to draw in the x direction (adjusted)
        Dim drawYDist As Double 'distance from center point to draw in the y direction (adjusted)
        Dim TransX As Double 'translated center polygon
        Dim TransY As Double '
        Dim p1 As Integer 'point of line to draw
        Dim p2 As Integer 'point 2 of line to draw
        Dim aPen As New Pen(Color.Aqua, 3)
        Dim tPen As New Pen(Color.Aqua, 0.5)
        'aPen.Alignment = Drawing2D.PenAlignment.Inset

        Dim YSlack As Integer 'extra space in y dimension
        'Dim StandIDToDraw As Integer

        'what is the size of the original display?
        FormWidth = panelMap.Size.Width
        FormHeight = panelMap.Size.Height
        FormWHRatio = FormWidth / FormHeight


        'DisplayMap = ZoomMapDisp
        CellWidth = DisplayMap.CellWidth
        'AngleBetweenCenters = DisplayMap.AngleBetweenCenters
        'HexHeight = 2 * 2 * (HexWidth / 2) / Math.Sqrt(3) 'expressed in original units
        MapWidth = (DisplayMap.MaxX - DisplayMap.MinX) + CellWidth 'have to pad for 1/2 hex on either side
        MapHeight = (DisplayMap.MaxY - DisplayMap.MinY) + CellWidth

        'logic: if map completely fits in form then have to scale map up
        '       if map is bigger than form in any direction, have to scale map down

        If MapWidth <= FormWidth And MapHeight <= FormHeight Then
            'need to scale up!
            AdjFactor = Math.Min(FormWidth / MapWidth, FormHeight / MapHeight)
            'map height is too big
        ElseIf MapHeight >= FormHeight And MapWidth <= FormWidth Then
            AdjFactor = FormHeight / MapHeight
            'map length is too big
        ElseIf MapHeight <= FormHeight And MapWidth >= FormWidth Then
            AdjFactor = FormWidth / MapWidth
        Else
            'both mapheight and mapwidth are too big
            AdjFactor = Math.Min(FormWidth / MapWidth, FormHeight / MapHeight)
        End If

        drawXDist = AdjFactor * CellWidth / 2 * Math.Sqrt(2)
        drawYDist = AdjFactor * CellWidth / 2 * Math.Sqrt(2)


        YSlack = FormHeight - MapHeight * AdjFactor 'keeps map at the top

        Dim points() As Drawing.PointF
        Dim DegAsRad45 As Double 'thirty degrees expressed in radians...
        'Dim SearchRad As Double = (drawXDist + drawYDist) / 2
        DegAsRad45 = Math.PI / 4

        NewBitMap = New Bitmap(FormWidth, FormHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
        g = Graphics.FromImage(NewBitMap)

        'Fill edge information
        FillQueryEdgeInfo(StandIDToDraw)

        'FILL THE REST OF THE POLYS IN THE STAND
        'If StandOrPoly = "Stand" Then
        SelectedArea = 0
        For jpoly As Integer = 1 To MxStandID
            QueryThisPoly = False
            For j As Integer = 1 To StandIDToDraw.Length - 1
                If GRIDPolys_QueryID(jpoly) = StandIDToDraw(j) Then
                    QueryThisPoly = True
                    Exit For
                End If
            Next
            If QueryThisPoly = True Then
                'ReDim points(3)
                TransX = drawXDist + (GRIDPolys_xVal(jpoly) - DisplayMap.MinX) * AdjFactor
                TransY = FormHeight - YSlack - drawYDist - (GRIDPolys_yVal(jpoly) - DisplayMap.MinY) * AdjFactor
                ReDim points(3)
                ' northeast - first point is the angle between centers + 45 degrees...
                points(1).X = Math.Round(TransX + Math.Cos(DegAsRad45) * drawYDist, 3) 'x is fine
                points(1).Y = Math.Round(TransY - Math.Sin(DegAsRad45) * drawYDist, 3)
                'northwest
                points(0).X = Math.Round(TransX + Math.Cos(3 * DegAsRad45) * drawYDist, 3)
                points(0).Y = Math.Round(TransY - Math.Sin(3 * DegAsRad45) * drawYDist, 3)
                'southwest
                points(3).X = Math.Round(TransX + Math.Cos(5 * DegAsRad45) * drawYDist, 3)
                points(3).Y = Math.Round(TransY - Math.Sin(5 * DegAsRad45) * drawYDist, 3)
                'southeast
                points(2).X = Math.Round(TransX + Math.Cos(7 * DegAsRad45) * drawYDist, 3)
                points(2).Y = Math.Round(TransY - Math.Sin(7 * DegAsRad45) * drawYDist, 3)

                g.FillPolygon(New SolidBrush(Color.FromArgb(SelectOpacity, 0, 255, 255)), points)
                'End If
                'If DrawEdge Then 'Assumption...these are indexed starting W counterclockwise
                For j As Integer = 0 To 7
                    If j = 0 Then
                        p1 = 0 'western edge
                        p2 = 3
                    ElseIf j = 2 Then
                        p1 = 3 'nw
                        p2 = 2 'ne
                    ElseIf j = 4 Then
                        p1 = 2 'ne
                        p2 = 1 'se
                    ElseIf j = 6 Then
                        p1 = 1 'se
                        p2 = 0 'sw
                    End If
                    'Pens.Aqua.Width = 3

                    If GRIDPolys_QueryEdge(jpoly, j) = 1 And (j = 0 Or j = 2 Or j = 4 Or j = 6) Then
                        g.DrawLine(aPen, points(p1), points(p2))
                    End If
                Next
                'g.DrawLine(tPen, points(1), points(3))
                SelectedArea += Acres(jpoly)
            End If
        Next


        'DelayTimeID = -99
        tsbClearSel.Enabled = True
        SelBitMap = NewBitMap
        DispMap = DisplayMap
        g.Dispose()
        aPen = Nothing
        panelMap.Invalidate()
    End Sub

    Private Function DetermineCellWidth() As Double
        'check from center x value to center x value to get the width of the hexagon
        'simplifying assumption...just use 1 hex, assume they are all the same size, then 
        'get distance between centers and the direction of the neighboring hexagons!

        'ASSUMPTION: Angle is determined in Q1 which assumes that there are at least 2 
        '  hexagons in the map and that at least two of them share a NW/SE border


        Dim j As Integer
        Dim width As Double
        Dim AdjCellInd As Integer 'hex index of the adjacent hexagon...
        Dim xdiff As Double 'difference in the x coords of centers
        Dim ydiff As Double 'difference in the y coords of centers

        Dim bCalculated As Boolean = False 'whether the hexagon is surrounded on all 6 sides

        DetermineCellWidth = 999999999

        'search for the first hexagon completely surrounded by 6 hexagons
        bCalculated = False
        For j = 1 To MxStandID
            For jside As Integer = 0 To 7
                AdjCellInd = PolyIndByPolyID(GRIDPolys_AdjCellInd(j, jside))
                If AdjCellInd <= 0 Then Exit For 'the jside...
                If AdjCellInd > 0 Then
                    If jside = 0 Or jside = 2 Or jside = 4 Or jside = 6 Then
                        ydiff = GRIDPolys_yVal(AdjCellInd) - GRIDPolys_yVal(j)
                        xdiff = GRIDPolys_xVal(AdjCellInd) - GRIDPolys_xVal(j)
                        'check for hexagon width
                        width = Math.Sqrt((xdiff) ^ 2 + Math.Abs(ydiff) ^ 2)
                        If Math.Round(width, 3) > 0 And width < DetermineCellWidth Then
                            DetermineCellWidth = width
                        End If
                        bCalculated = True
                        Exit For
                    End If
                End If
            Next jside
            If bCalculated = True Then Exit For 'don't have to go through all hexagons...
        Next j

    End Function

    Private Sub FillEdgeInfo() '(ByVal CellWidth As Double)
        Dim j As Integer
        Dim k As Integer
        Dim AdjCellInd As Integer 'hex index of the adjacent hexagon...

        ''for edge information...index values in the zoommap subroutine
        '' 3(east) is the positive angle in the NE direction
        ''  0 = W
        ''  1 = SW
        ''  2 = S
        ''  3 = SE
        ''  4 = E
        ''  5 = NE
        ''  6 = N
        ''  7 = NW
        '' Edge() value of 0 means it IS edge...
        ReDim GRIDPolys_Edge(MxStandID, 7)
        For j = 1 To MxStandID
            k = -1
            If FireEventEdgeQual = -1 Then k = 3
            For jside As Integer = 0 To 7
                k = k + (1 * FireEventEdgeQual)
                If k < 0 Then k = 7
                'jside = jside + 1
                'default
                GRIDPolys_Edge(j, k) = 1 'yes it is an edge
                AdjCellInd = PolyIndByPolyID(GRIDPolys_AdjCellInd(j, jside))
                If AdjCellInd > 0 Then

                    If GRIDPolys_DisplayID(j) = GRIDPolys_DisplayID(AdjCellInd) Then
                        'If jside = 0 Or jside = 2 Or jside = 4 Or jside = 6 Then GRIDPolys_Edge(j, k) = 0
                        GRIDPolys_Edge(j, k) = 0
                    End If

                End If
            Next jside
        Next j

    End Sub
    Private Sub FillQueryEdgeInfo(ByVal StandIDToDraw() As String) '(ByVal CellWidth As Double)
        Dim j As Integer
        Dim k As Integer
        Dim AdjCellInd As Integer 'hex index of the adjacent hexagon...
        Dim bQueryCenter As Boolean
        Dim bQueryAdj As Boolean

        ''for edge information...index values in the zoommap subroutine
        '' 3(east) is the positive angle in the NE direction
        ''  0 = W
        ''  1 = SW
        ''  2 = S
        ''  3 = SE
        ''  4 = E
        ''  5 = NE
        ''  6 = N
        ''  7 = NW
        '' Edge() value of 0 means it IS edge...


        ReDim GRIDPolys_QueryEdge(MxStandID, 7)
        For j = 1 To MxStandID
            bQueryCenter = False
            bQueryAdj = False
            For jid As Integer = 1 To StandIDToDraw.Length - 1
                If GRIDPolys_QueryID(j) = StandIDToDraw(jid) Then
                    bQueryCenter = True
                    Exit For
                End If
            Next

            k = -1
            If FireEventEdgeQual = -1 Then k = 3
            For jside As Integer = 0 To 7
                bQueryAdj = False
                k = k + (1 * FireEventEdgeQual)
                If k < 0 Then k = 7
                'jside = jside + 1
                'default
                GRIDPolys_QueryEdge(j, k) = 1 'yes it is an edge
                AdjCellInd = PolyIndByPolyID(GRIDPolys_AdjCellInd(j, jside))
                If AdjCellInd > 0 Then
                    For jid As Integer = 1 To StandIDToDraw.Length - 1
                        If GRIDPolys_QueryID(AdjCellInd) = StandIDToDraw(jid) Then
                            bQueryAdj = True
                            Exit For
                        End If
                    Next

                    If bQueryCenter = bQueryAdj Then
                        'If jside = 0 Or jside = 2 Or jside = 4 Or jside = 6 Then GRIDPolys_Edge(j, k) = 0
                        GRIDPolys_QueryEdge(j, k) = 0
                    End If

                End If
            Next jside
        Next j

    End Sub
    Private Sub FillFireEdgeInfo(ByVal kper As Integer)
        Dim j As Integer
        Dim k As Integer
        Dim kside As Integer
        Dim AdjCellInd As Integer 'poly index of the adjacent polygon...

        'FireEventEdgeQual = 1 'default; set to match these below
        ''for edge information...index values in the zoommap subroutine
        '' 3(east) is the positive angle in the NE direction
        ''  0 = W
        ''  1 = SW
        ''  2 = S
        ''  3 = SE
        ''  4 = E
        ''  5 = NE
        ''  6 = N
        ''  7 = NW
        '' Edge() value of 0 means it IS edge...
        ReDim FireEventEdge(MxStandID, 4)
        ReDim FireOrigin(MxStandID)

        For j = 1 To MxStandID
            k = 0
            If FireEventEdgeQual = -1 Then k = 3
            For jside As Integer = -1 To 6
                k = k + (1 * FireEventEdgeQual)
                If k < 1 Then k = 4
                jside = jside + 1
                AdjCellInd = PolyIndByPolyID(GRIDPolys_AdjCellInd(j, jside))
                If FireEvent(j, kper) = j Then FireOrigin(j) = 1
                FireEventEdge(j, k) = 1 'default edge = yes
                If FireEvent(j, kper) = 0 And FireEvent(GRIDPolys_PolyID(AdjCellInd), kper) = 0 Then FireEventEdge(j, k) = 0 'if this is not a fire, edge  = no
                If AdjCellInd > 0 Then

                    If FireEvent(j, kper) = FireEvent(GRIDPolys_PolyID(AdjCellInd), kper) Then ' And FireEvent(j, kper) > 0 Then
                        FireEventEdge(j, k) = 0 'edge = no
                        'If jside = 0 Or jside = 2 Or jside = 4 Or jside = 6 Then GRIDPolys_Edge(j, jside) = 1
                    End If
                    'if the adjacent cell is an origin, don't draw a fire event edge here
                    If FireEvent(GRIDPolys_PolyID(AdjCellInd), kper) = GRIDPolys_PolyID(AdjCellInd) Then FireEventEdge(j, k) = 0

                End If
            Next jside
        Next j
    End Sub

    Private Sub HexMap_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Dim ZoomMap As MapInfo

        ResetTSButtons(False)
        ReDim bPainted(NWindows)
        ReDim WindStageBitMap(NWindows)
        ReDim BitMapExtent(NWindows)
        BitMapExtent(CurrWindInd) = DispMap

        bWindStage = True
        If bMapLoaded = True And panelMap.Size.Width > 0 And panelMap.Size.Height > 0 Then
            ZoomMap.MinX = 0
            ZoomMap.MaxX = panelMap.Size.Width
            ZoomMap.MinY = 0
            ZoomMap.MaxY = panelMap.Size.Height

            ZoomBitmap(ZoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, False)
        End If
        If bSelected = True And panelMap.Size.Width > 0 And panelMap.Size.Height > 0 Then RedrawSelected()
        If bHoldIdentify = True And panelMap.Size.Width > 0 And panelMap.Size.Height > 0 Then RedrawIdentified()
        
        'Me.Refresh()
    End Sub
    Private Sub HexMap_Unload(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        If bMapLoaded = True Then bMapLoaded = False
    End Sub


    Private Sub panelMap_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles panelMap.Paint

        If bMapLoaded = True Then
            'If bWindStage = False Then
            e.Graphics.Clear(Color.Gray)
            'bFlashID = bHoldIdentify
            If bWindStage = False Then e.Graphics.DrawImage(PaintBitMap, 0, 0)
            If bWindStage = True Then e.Graphics.DrawImage(WindStageBitMap(CurrWindInd), 0, 0)
            If bFlashID = True Then e.Graphics.DrawImage(IDBitMap, 0, 0)
            e.Graphics.DrawImage(DisplayEdgeBitMap, 0, 0)
            If bFlashSel = True Then e.Graphics.DrawImage(SelBitMap, 0, 0)


        End If
    End Sub
    Private Sub panelMap_mousedown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles panelMap.MouseDown
        If bMapLoaded = False Then Exit Sub

        bmpX1 = e.X
        bmpY1 = e.Y

        'zoom command
        If tsbZoomIn.Checked Then
            ' Make a note that we "have the mouse".
            bHaveMouse = True
            ' Store the "starting point" for this rubber-band rectangle.
            ptOriginal.X = e.X
            ptOriginal.Y = e.Y
            ' Special value lets us know that no previous
            ' rectangle needs to be erased.
            ptLast.X = -1
            ptLast.Y = -1
            panelMap.Cursor = Cursors.Cross
        End If

        'pan command
        If tsbPan.Checked Then
            panelMap.Cursor = Cursors.Hand

        End If

        'ID Command
        If tsbID.Checked Then
            panelMap.Cursor = Cursors.UpArrow
        End If


    End Sub
    Private Sub panelMap_mousemove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles panelMap.MouseMove
        Dim ptCurrent As Point
        ptCurrent.X = e.X
        ptCurrent.Y = e.Y

        ' If we "have the mouse", and we are zooming then we draw our lines.
        If (bHaveMouse) And tsbZoomIn.Checked Then
            ' If we have drawn previously, draw again in
            ' that spot to remove the lines.
            If (ptLast.X <> -1) Then
                MyDrawReversibleRectangle(ptOriginal, ptLast)
            End If
            ' Update last point.
            ptLast = ptCurrent
            ' Draw new lines.
            MyDrawReversibleRectangle(ptOriginal, ptCurrent)
        End If

    End Sub
    Private Sub panelMap_mouseup(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles panelMap.MouseUp
        If bMapLoaded = False Then Exit Sub
        'Dim Atts() As String
        Dim pixthresh As Integer = 10
        Dim kID As String 'either poly ID or stand ID
        bmpX2 = e.X
        bmpY2 = e.Y

        If tsbZoomIn.Checked Then

            ' Set internal flag to know we no longer "have the mouse".
            bHaveMouse = False
            ' If we have drawn previously, draw again in that spot
            ' to remove the lines.
            If (ptLast.X <> -1) Then
                Dim ptCurrent As Point
                ptCurrent.X = e.X
                ptCurrent.Y = e.Y
                MyDrawReversibleRectangle(ptOriginal, ptLast)
            End If
            ' Set flags to know that there is no "previous" line to reverse.
            ptLast.X = -1
            ptLast.Y = -1
            ptOriginal.X = -1
            ptOriginal.Y = -1


            'CONTROL FOR WORTHLESS CLICKS

            If Math.Abs(bmpX1 - bmpX2) > pixthresh And Math.Abs(bmpY1 - bmpY2) > pixthresh Then
                bWindStage = False
                'bHoldIdentify = False
                Dim zoomMap As MapInfo
                zoomMap.MaxX = Math.Max(bmpX1, bmpX2)
                zoomMap.MinX = Math.Min(bmpX1, bmpX2)
                'y-direction is reversed
                zoomMap.MinY = Math.Min(bmpY1, bmpY2)
                zoomMap.MaxY = Math.Max(bmpY1, bmpY2)

                ZoomBitmap(zoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, False) ', CurrWindInd)
                CurrHistInd = CurrHistInd + 1
                If CurrHistInd > MaxHistory Then
                    RefreshHistory(MapHistory)
                    CurrHistInd = MaxHistory
                End If
                If CurrHistInd > 0 Then tsbPrev.Enabled = True
                MapHistory(CurrHistInd) = DispMap
            End If
        End If

        'pan command
        If tsbPan.Checked Then

            bWindStage = False
            'bHoldIdentify = False
            Dim zoomMap As MapInfo
            zoomMap.MaxX = panelMap.Width + (bmpX1 - bmpX2) ' Math.Max(bmpX1, bmpX2)
            zoomMap.MinX = 0 + (bmpX1 - bmpX2) 'Math.Min(bmpX1, bmpX2)
            'y-direction is reversed
            zoomMap.MinY = 0 + (bmpY1 - bmpY2) 'Math.Min(bmpY1, bmpY2)
            zoomMap.MaxY = panelMap.Height + (bmpY1 - bmpY2)  ' Math.Max(bmpY1, bmpY2)

            ZoomBitmap(zoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, True) ', CurrWindInd)
            CurrHistInd = CurrHistInd + 1

            If CurrHistInd > MaxHistory Then
                RefreshHistory(MapHistory)
                CurrHistInd = MaxHistory
            End If
            If CurrHistInd > 0 Then tsbPrev.Enabled = True
            MapHistory(CurrHistInd) = DispMap
        End If

        'ID command
        If tsbID.Checked Then
            Dim kpoly As Integer
            If IdentifyField = "" Then
                IdentifyField = DisplayField
                PopulateIdentifyID(IdentifyField)
            End If
            'search for the Stand ID and number of prescriptions of the poly with the center closest to the clicked point on the map
            Dim idarea As Double
            'bHoldIdentify = frmStandAtts.cbHoldIdentify.Checked

            DetermineIdentifyValue(DispMap, bmpX1, bmpY1, iIdnID, kpoly)

            bFlashID = bHoldIdentify
            If bHoldIdentify = False Then
                IdentifyMapSpace(DispMap, iIdnID, idarea)
                bFlashID = True
                Me.Refresh()
                Delay(0.5)
                bFlashID = False
                'Me.Refresh()

            End If
            kID = GRIDPolys_IdentifyID(kpoly)
            Label6.Text = IdentifyField & " ID: " & kID
            gStandID = kID
            ReDim Atts(NumAtts + 1)
            For jatt As Integer = 1 To NumAtts - 2
                Atts(jatt) = PolyAtts(kpoly, jatt)
            Next
            Atts(NumAtts - 1) = LegendColor(iStandColor(CurrWindInd, kpoly)).LabelDispStr
            Atts(NumAtts) = idarea

            If bIDClosed = True Then
                attfrm.Close()
                attfrm = New frmStandAtts
                attfrm.Show()
                'Me.Refresh()
            Else
                attfrm.UpdateID()
                'attfrm.Show()
                attfrm.BringToFront()
                'Me.Refresh()
            End If
        End If

        If bSelected = True Then RedrawSelected()

        If bHoldIdentify = True Then RedrawIdentified()

        panelMap.Cursor = Cursors.Arrow
        Me.Refresh()

    End Sub

    Private Sub tsbFullExtent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbFullExtent.Click

        bWindStage = False
        Dim ZoomMap As MapInfo
        DispMap = FullMap
        Dim bNewHist As Boolean = False
        'ResetTSButtons(False)
        If bMapLoaded = True Then
            ZoomMap.MinX = 0
            ZoomMap.MaxX = panelMap.Size.Width
            ZoomMap.MinY = 0
            ZoomMap.MaxY = panelMap.Size.Height

            ZoomBitmap(ZoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, False) ', CurrWindInd)

            'log a new history extent only if the window has moved

            If BitMapExtent(CurrWindInd).MaxX <> DispMap.MaxX Then bNewHist = True
            If BitMapExtent(CurrWindInd).MinX <> DispMap.MinX Then bNewHist = True
            If BitMapExtent(CurrWindInd).MaxY <> DispMap.MaxY Then bNewHist = True
            If BitMapExtent(CurrWindInd).MinY <> DispMap.MinY Then bNewHist = True

            If bNewHist = True Then
                CurrHistInd = CurrHistInd + 1
                If CurrHistInd > MaxHistory Then
                    RefreshHistory(MapHistory)
                    CurrHistInd = MaxHistory
                End If
                MapHistory(CurrHistInd) = DispMap
            End If
            If CurrHistInd > 1 Then tsbPrev.Enabled = True

        End If
        If bSelected = True Then RedrawSelected()

        If bHoldIdentify = True Then RedrawIdentified()

        Me.Refresh()
    End Sub
    Private Sub tsbPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbPrev.Click
        If bMapLoaded = False Then Exit Sub
        If CurrHistInd > 1 Then CurrHistInd = CurrHistInd - 1
        bWindStage = False
        Dim ZoomMap As MapInfo
        DispMap = MapHistory(CurrHistInd)
        'ResetTSButtons(False)
        If bMapLoaded = True Then
            ZoomMap.MinX = 0
            ZoomMap.MaxX = panelMap.Size.Width
            ZoomMap.MinY = 0
            ZoomMap.MaxY = panelMap.Size.Height

            ZoomBitmap(ZoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, False) ', CurrWindInd)
            If bSelected = True Then RedrawSelected()
            If bHoldIdentify = True Then RedrawIdentified()
        End If
        If CurrHistInd > 1 Then tsbPrev.Enabled = True
        If CurrHistInd <= 1 Then tsbPrev.Enabled = False

    End Sub

    ' Convert and Normalize the points and draw the reversible frame.
    Private Sub MyDrawReversibleRectangle(ByVal p1 As Point, ByVal p2 As Point)
        Dim rc As Rectangle
        ' Convert the points to screen coordinates.
        p1 = panelMap.PointToScreen(p1)
        p2 = panelMap.PointToScreen(p2)
        ' Normalize the rectangle.
        If (p1.X < p2.X) Then
            rc.X = p1.X
            rc.Width = p2.X - p1.X
        Else
            rc.X = p2.X
            rc.Width = p1.X - p2.X
        End If
        If (p1.Y < p2.Y) Then
            rc.Y = p1.Y
            rc.Height = p2.Y - p1.Y
        Else
            rc.Y = p2.Y
            rc.Height = p1.Y - p2.Y
        End If
        ' Draw the reversible frame.
        ControlPaint.DrawReversibleFrame(rc, Color.Red, FrameStyle.Thick)
    End Sub

    Private Sub ToolStripPan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbPan.Click
        Dim b As Boolean
        If tsbPan.Checked Then b = True
        If tsbPan.Checked = False Then b = False
        ResetTSButtons(False)
        tsbPan.Checked = b
    End Sub

    Private Sub ToolStripZoom_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbZoomIn.Click
        Dim b As Boolean
        If tsbZoomIn.Checked Then b = True
        If tsbZoomIn.Checked = False Then b = False
        ResetTSButtons(False)
        tsbZoomIn.Checked = b
    End Sub

    Private Sub ToolStripInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbID.Click
        Dim b As Boolean
        If tsbID.Checked Then b = True
        If tsbID.Checked = False Then b = False
        ResetTSButtons(False)
        tsbID.Checked = b
        If bIDClosed = True Then
            attfrm.Close()
            attfrm = New frmStandAtts
            attfrm.Show()
            'Me.Refresh()
        Else
            attfrm.UpdateID()
            'attfrm.Show()
            attfrm.BringToFront()
            'Me.Refresh()
        End If

    End Sub

    Private Sub ResetTSButtons(ByVal bPress As Boolean)
        tsbZoomIn.Checked = bPress
        tsbPan.Checked = bPress
        tsbID.Checked = bPress
    End Sub

    Private Sub RefreshHistory(ByRef History() As MapInfo)
        For jhist As Integer = 1 To MaxHistory - 1
            History(jhist) = History(jhist + 1)
        Next
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'If rbWindow.Checked Then
        CurrWindInd = CurrWindInd + 1

        If CurrWindInd > NWindows Then
            CurrWindInd = 0
        End If

        MoveWindow(CurrWindInd)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        CurrWindInd = CurrWindInd - 1
        If CurrWindInd < 0 Then
            CurrWindInd = NWindows
        End If


        MoveWindow(CurrWindInd)
    End Sub


    Private Sub MoveWindow(ByVal Currwindind As Integer) ', ByVal CurrStageInd As Integer)
        If bMapLoaded = False Then
            Exit Sub
        End If
        Dim ZoomMap As MapInfo
        'code the color attribute of the HexPolys with the current map information

        tbCurrWindInd.Text = Currwindind 'Windows_TimePeriodLabel(Currwindind)
        Label4.Text = Windows_Label(Currwindind) '(Currwindind)
        rtbAcres.Text = ""

        For jcol As Integer = 1 To NColors
            If Windows_Acres(Currwindind, jcol) > 0 Then
                rtbAcres.Text = rtbAcres.Text & LegendColor(jcol).LabelDispStr & Chr(13)
                rtbAcres.Text = rtbAcres.Text & "  " & Math.Round(Windows_Acres(Currwindind, jcol), 0) & Chr(13)

            End If
        Next 'jatt

        Dim bExtentMoved As Boolean = False
        Dim bNewHist As Boolean = False

        bWindStage = True


        'has the image shifted?
        If BitMapExtent(Currwindind).MaxX <> DispMap.MaxX Then bExtentMoved = True
        If BitMapExtent(Currwindind).MinX <> DispMap.MinX Then bExtentMoved = True
        If BitMapExtent(Currwindind).MaxY <> DispMap.MaxY Then bExtentMoved = True
        If BitMapExtent(Currwindind).MinY <> DispMap.MinY Then bExtentMoved = True
        If BitMapExtent(Currwindind).bDrawEdges <> bDrawEdges Or BitMapExtent(Currwindind).bDrawFireEdges <> bDrawFireEvents Or bDrawNewEdges = True Then
            bExtentMoved = True
            'BitMapExtent(Currwindind, CurrStageInd).bDrawEdges = bDrawEdges
            bDrawNewEdges = False 'immediately clear this setting
            'bHoldIdentify = False
        End If

        If bExtentMoved Or bPainted(Currwindind) = False Then
            'the palette is the full screen size - there is no actual zoom
            ZoomMap.MinX = 0
            ZoomMap.MaxX = panelMap.Size.Width
            ZoomMap.MinY = 0
            ZoomMap.MaxY = panelMap.Size.Height

            ZoomBitmap(ZoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, False) ', Currwindind)
            BitMapExtent(Currwindind) = DispMap
            If bSelected = True Then RedrawSelected()
            If bHoldIdentify = True Then RedrawIdentified()

        End If
        If bExtentMoved Then

            'log a new history extent only if the window has moved

            If BitMapExtent(Currwindind).MaxX <> MapHistory(CurrHistInd).MaxX Then bNewHist = True
            If BitMapExtent(Currwindind).MinX <> MapHistory(CurrHistInd).MinX Then bNewHist = True
            If BitMapExtent(Currwindind).MaxY <> MapHistory(CurrHistInd).MaxY Then bNewHist = True
            If BitMapExtent(Currwindind).MinY <> MapHistory(CurrHistInd).MinY Then bNewHist = True
            If BitMapExtent(Currwindind).bDrawEdges <> bDrawEdges Then
                bNewHist = True
                BitMapExtent(Currwindind).bDrawEdges = bDrawEdges
            End If
            If BitMapExtent(Currwindind).bDrawFireEdges <> bDrawFireEvents Then
                bNewHist = True
                BitMapExtent(Currwindind).bDrawFireEdges = bDrawFireEvents
            End If

            If bNewHist = True Then
                CurrHistInd = CurrHistInd + 1
                If CurrHistInd > MaxHistory Then
                    RefreshHistory(MapHistory)
                    CurrHistInd = MaxHistory
                End If
                MapHistory(CurrHistInd) = DispMap
            End If
            If CurrHistInd > 1 Then tsbPrev.Enabled = True

        End If
        'End If



        Dim bmp As New Bitmap(fullPanel.Width, fullPanel.Height)
        Using g As Graphics = Graphics.FromImage(bmp)
            g.CopyFromScreen(fullPanel.PointToScreen(New Point(0, 0)), New Point(0, 0), New Size(fullPanel.Width, fullPanel.Height))
        End Using
        'fullPanel.DrawToBitmap(bmp, fullPanel.DisplayRectangle)
        FullProgramBitMap(Currwindind) = bmp
        Me.Refresh()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'DelayTimeID = 0.5
        If Play = False Then
            Play = True
            Button1.Image = ImageList2.Images(1)
        Else
            Play = False
            Button1.Image = ImageList2.Images(0)
        End If

        Do Until Play = False

            CurrWindInd = CurrWindInd + 1
            If CurrWindInd > NWindows Then CurrWindInd = 0
            MoveWindow(CurrWindInd)
            Delay(PlayDelayTime)

        Loop

    End Sub

    Sub Delay(ByVal dblSecs As Double)

        Const OneSec As Double = 1.0# / (1440.0# * 60.0#)
        Dim dblWaitTil As Date
        Now.AddSeconds(OneSec)
        dblWaitTil = Now.AddSeconds(OneSec).AddSeconds(dblSecs)
        Do Until Now > dblWaitTil
            Application.DoEvents() ' Allow windows messages to be processed
        Loop

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbSelect.Click
        If bMapLoaded = False Then Exit Sub
        If bSelectorShowing = True Then Exit Sub
        NSelectedID = 1
        ReDim SelectedID(NSelectedID)
        SelectedID(NSelectedID) = "-9999"

        Dim sel As New Form
        sel = New MapSelector
        sel.Show

    End Sub
    Public Sub DoSelect()
        On Error GoTo Handler


        Dim NSel As Integer = 0

        ReDim iSelID(NSelectedID)
        For j As Integer = 1 To NSelectedID
            If SelectedID(j) <> "-9999" And Trim(SelectedID(j)) <> "" Then
                iSelID(j) = Trim(SelectedID(j))
                NSel = NSel + 1
            End If

        Next
        If NSel > 0 Then
            bSelected = True
            'search for the Stand ID  of the poly with the center closest to the clicked point on the map
            Dim selarea As Double = 0
            QueryMapSpace(DispMap, iSelID, selarea)
            bFlashSel = True
            Label6.Text = QueryField & " ID: " & iSelID(1)
            Label2.Text = "Area: " & Math.Round(selarea, 0)
            Me.Refresh()
        Else
            tsbClearSel.Enabled = False
            bFlashSel = False
            bSelected = False
            ReDim iSelID(0)
            Me.Refresh()
        End If
        Exit Sub
Handler:
        MsgBox("Error in selection criteria" & Chr(13) & "Ensure values are " & Chr(13) & "Separated by commas")
    End Sub
    Public Sub PopSelectorItems(ByVal MaxItems As Integer)
        'need to populate the list of potential values to select

        Dim temparr() As Object
        NSelectorItems = 0
        ReDim temparr(NSelectorItems)
        UniqueItems(GRIDPolys_QueryID, temparr, NSelectorItems, MaxItems)
        ReDim SelectorItems(NSelectorItems)
        For j As Integer = 1 To NSelectorItems
            SelectorItems(j) = temparr(j)
        Next
        If NSelectorItems < 1000 Then
            Array.Sort(SelectorItems)
        End If
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAddTheme.Click
        Dim OpenFileDialog1 As New OpenFileDialog
        With OpenFileDialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the file with stand attributes, x/y coordinate (or row/column) info (e.g., .attributesall)"
            '.Filter = "Text Files(.txt) |*.txt"
            If .ShowDialog = System.Windows.Forms.DialogResult.Cancel Then Return
        End With

        MapPolyFile = OpenFileDialog1.FileName

        With OpenFileDialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select file with stand-by-stand outputs by time period"
            .Filter = "Text Files(.txt, .csv) |*.txt; *.csv"
            If .ShowDialog = System.Windows.Forms.DialogResult.Cancel Then Return
        End With

        OutputByPeriodFile = OpenFileDialog1.FileName

        'the color index file
        bColorFile = True
        With OpenFileDialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the file with color information. 'Cancel' to define later."
            .Filter = "Color Files(.clr) |*.clr"
            If .ShowDialog = System.Windows.Forms.DialogResult.Cancel Then bColorFile = False
        End With
        ColorIndexFile = "NeedToSave"
        If bColorFile = True Then ColorIndexFile = OpenFileDialog1.FileName

        'got to here - so set the fireeventedgequal
        FireEventEdgeQual = 1

        lblFileName.Text = OutputByPeriodFile
        GridFileType = "XY"

        bMapLoaded = False
        bSelected = False
        bHoldIdentify = False
        tsbFireEvents.Enabled = True

        AddTheme()

        Dim ZoomMap As MapInfo
        ResetTSButtons(False)
        If bMapLoaded = True Then
            ZoomMap.MinX = 0
            ZoomMap.MaxX = panelMap.Size.Width
            ZoomMap.MinY = 0
            ZoomMap.MaxY = panelMap.Size.Height

            ZoomBitmap(ZoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, False)
            If CurrHistInd > MaxHistory Then RefreshHistory(MapHistory)
            MapHistory(CurrHistInd) = DispMap
        End If

        Me.Refresh()
        bHaveMouse = False
        DisplayField = tsbcbbStandPoly.Text
        PopulateDispID(DisplayField)

    End Sub

    Private Sub tsbAddSIMPPLLERun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAddSIMPPLLERun.Click

        bCancel = False
        Dim RunForm As New frmSIMPPLLE_Results
        RunForm.ShowDialog()
        RunForm = Nothing

        If bCancel = True Then
            bCancel = False
            Return
        End If
        bSelected = False
        bHoldIdentify = False
        FireEventEdgeQual = 1

        lblFileName.Text = OutputByPeriodFile
        GridFileType = "SIMPPLLE"

        bMapLoaded = False
        tsbFireEvents.Enabled = True

        AddTheme()

        Dim ZoomMap As MapInfo
        ResetTSButtons(False)
        If bMapLoaded = True Then
            ZoomMap.MinX = 0
            ZoomMap.MaxX = panelMap.Size.Width
            ZoomMap.MinY = 0
            ZoomMap.MaxY = panelMap.Size.Height

            ZoomBitmap(ZoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, False)
            If CurrHistInd > MaxHistory Then RefreshHistory(MapHistory)
            MapHistory(CurrHistInd) = DispMap
        End If

        Me.Refresh()
        bHaveMouse = False
        DisplayField = tsbcbbStandPoly.Text
        PopulateDispID(DisplayField)
    End Sub

    Private Sub tsbAddGrid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim OpenFileDialog1 As New OpenFileDialog
        With OpenFileDialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the GRID file"
            .Filter = "Text Files(.txt) |*.txt"
            If .ShowDialog = System.Windows.Forms.DialogResult.Cancel Then Return
        End With

        MapPolyFile = OpenFileDialog1.FileName

        With OpenFileDialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select GRID value file"
            .Filter = "CSV Files(.csv) |*.csv"
            If .ShowDialog = System.Windows.Forms.DialogResult.Cancel Then Return
        End With

        OutputByPeriodFile = OpenFileDialog1.FileName

        'the color index file
        With OpenFileDialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the file with color information"
            .Filter = "Color Files(.clr) |*.clr"
            If .ShowDialog = System.Windows.Forms.DialogResult.Cancel Then Return
        End With
        ColorIndexFile = OpenFileDialog1.FileName
        OpenFileDialog1 = Nothing

        lblFileName.Text = OutputByPeriodFile
        GridFileType = "GRID"

        bMapLoaded = False
        bSelected = False
        bHoldIdentify = False
        AddTheme()

        Dim ZoomMap As MapInfo
        ResetTSButtons(False)
        If bMapLoaded = True Then
            ZoomMap.MinX = 0
            ZoomMap.MaxX = panelMap.Size.Width
            ZoomMap.MinY = 0
            ZoomMap.MaxY = panelMap.Size.Height

            ZoomBitmap(ZoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, False)
            If CurrHistInd > MaxHistory Then RefreshHistory(MapHistory)
            MapHistory(CurrHistInd) = DispMap
        End If

        'Me.Refresh()
        'bHaveMouse = False
    End Sub
    Private Sub txbFixedZoomIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbFixedZoomIn.Click
        Dim adjfactor As Double = 0.8
        If bMapLoaded = False Then Exit Sub

        bWindStage = False
        'bHoldIdentify = False
        Dim ZoomMap As MapInfo

        Dim bNewHist As Boolean = False
        'ResetTSButtons(False)
        If bMapLoaded = True Then
            ZoomMap.MinX = 0 + 0.5 * (panelMap.Size.Width - panelMap.Size.Width * adjfactor)
            ZoomMap.MaxX = panelMap.Size.Width - 0.5 * (panelMap.Size.Width - panelMap.Size.Width * adjfactor)
            ZoomMap.MinY = 0 + 0.5 * (panelMap.Size.Height - panelMap.Size.Height * adjfactor)
            ZoomMap.MaxY = panelMap.Size.Height - 0.5 * (panelMap.Size.Height - panelMap.Size.Height * adjfactor)

            ZoomBitmap(ZoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, True) ', CurrWindInd)

            'log a new history extent only if the window has moved

            If BitMapExtent(CurrWindInd).MaxX <> DispMap.MaxX Then bNewHist = True
            If BitMapExtent(CurrWindInd).MinX <> DispMap.MinX Then bNewHist = True
            If BitMapExtent(CurrWindInd).MaxY <> DispMap.MaxY Then bNewHist = True
            If BitMapExtent(CurrWindInd).MinY <> DispMap.MinY Then bNewHist = True

            If bNewHist = True Then
                tsbPrev.Enabled = True
                CurrHistInd = CurrHistInd + 1
                If CurrHistInd > MaxHistory Then
                    RefreshHistory(MapHistory)
                    CurrHistInd = MaxHistory
                End If
                MapHistory(CurrHistInd) = DispMap
            End If
            If CurrHistInd > 1 Then tsbPrev.Enabled = True

        End If
        If bSelected = True Then RedrawSelected()
        If bHoldIdentify = True Then RedrawIdentified()

        Me.Refresh()

    End Sub
    Private Sub tsbFixedZoomOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbFixedZoomOut.Click
        Dim adjfactor As Double = 0.8
        If bMapLoaded = False Then Exit Sub

        bWindStage = False
        'bHoldIdentify = False
        Dim ZoomMap As MapInfo

        Dim bNewHist As Boolean = False
        'ResetTSButtons(False)
        If bMapLoaded = True Then
            ZoomMap.MinX = 0 - 0.5 * (panelMap.Size.Width - panelMap.Size.Width * adjfactor)
            ZoomMap.MaxX = panelMap.Size.Width + 0.5 * (panelMap.Size.Width - panelMap.Size.Width * adjfactor)
            ZoomMap.MinY = 0 - 0.5 * (panelMap.Size.Height - panelMap.Size.Height * adjfactor)
            ZoomMap.MaxY = panelMap.Size.Height + 0.5 * (panelMap.Size.Height - panelMap.Size.Height * adjfactor)

            ZoomBitmap(ZoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, True) ', CurrWindInd)

            'log a new history extent only if the window has moved

            If BitMapExtent(CurrWindInd).MaxX <> DispMap.MaxX Then bNewHist = True
            If BitMapExtent(CurrWindInd).MinX <> DispMap.MinX Then bNewHist = True
            If BitMapExtent(CurrWindInd).MaxY <> DispMap.MaxY Then bNewHist = True
            If BitMapExtent(CurrWindInd).MinY <> DispMap.MinY Then bNewHist = True

            If bNewHist = True Then
                tsbPrev.Enabled = True
                CurrHistInd = CurrHistInd + 1
                If CurrHistInd > MaxHistory Then
                    RefreshHistory(MapHistory)
                    CurrHistInd = MaxHistory
                End If
                MapHistory(CurrHistInd) = DispMap
            End If
            If CurrHistInd > 1 Then tsbPrev.Enabled = True

        End If
        If bSelected = True Then RedrawSelected()
        If bHoldIdentify = True Then RedrawIdentified()

        Me.Refresh()
    End Sub
    Private Sub tsbClearSel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbClearSel.Click
        tsbClearSel.Enabled = False
        bFlashSel = False
        bSelected = False
        ReDim iSelID(0)
        Me.Refresh()
    End Sub

    Private Sub cbDrawEdge_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDrawEdge.CheckedChanged
        bDrawEdges = cbDrawEdge.Checked
        If bMapLoaded = True Then MoveWindow(CurrWindInd)
        'Me.Refresh()
    End Sub
    Private Sub cbFireEvents_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbFireEvents.CheckedChanged
        bDrawFireEvents = cbFireEvents.Checked
        If bMapLoaded = True Then MoveWindow(CurrWindInd)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim bloaded As Boolean
        bloaded = System.IO.File.Exists(ColorIndexFile)
        If bloaded = False Then
            MsgBox("Color File Not Loaded")
            Exit Sub
        End If
        bCancel = False

        Dim colorform As New Colors
        colorform.ShowDialog()
        colorform = Nothing

        If bCancel = False Then
            LoadWindowColorInfo(ColorIndexFile)
            ReDefineMapColors()
            For jwind As Integer = 0 To NWindows
                bPainted(jwind) = False
            Next
            If bMapLoaded = True Then MoveWindow(CurrWindInd)
        End If
    End Sub
    Private Sub ReDefineMapColors()
        Dim PerOutValue As Single
        Dim kwind As Integer

        'read windows file again and fill period-by-period value
        Dim windmultval As Integer
        Dim kval As Integer

        ReDim Windows_Acres(NWindows, NColors)
        'ReDim Value_Colored(NUniqueValues)
        For jstand As Integer = 1 To MxStandID
            'check to see if the stand is in a window...
            For jwind As Integer = 0 To NWindows
                'If jwind = 2 And jstand = 119732 Then Stop
                'each stand will have a color for each stage of each window...
                kwind = jwind
                'If PolyFileType = "GRID" Then kwind = 0
                PerOutValue = Windows_Value(jwind, jstand)
                iStandColor(jwind, jstand) = 0 'default is non-forest
                'If bShowAcres = True Then
                windmultval = PerOutValue 'Windows_Value(jwind, jstand)
                'find kval
                kval = 0 'default
                For jcol As Integer = 1 To NColors
                    If PerOutValue >= LegendColor(jcol).LabelMinVal And PerOutValue < LegendColor(jcol).LabelMaxVal Then
                        kval = LegendColor(jcol).LabelColorInd
                        'kcolm = GetStrIndex(ColmName, LegendColor(jcol).ColumnName)
                        Exit For
                    End If
                Next jcol
                'If kval > MaxLabelAttValue(kcolm) Then kval = 0 'was not defined
                If windmultval > 1 Then windmultval = 1 'catch for indices greater than 1
                If kval > 0 Then windmultval = 1 'catch if PerOutVal is 0 and the lowest defined class starts at 0
                Windows_Acres(jwind, kval) = Windows_Acres(jwind, kval) + windmultval * Acres(jstand)
                'End If
                'also need to populate the LabelValue on this....
                For jcol As Integer = 1 To LegendColor.Length - 1
                    If PerOutValue >= LegendColor(jcol).LabelMinVal And PerOutValue < LegendColor(jcol).LabelMaxVal Then
                        iStandColor(jwind, jstand) = jcol
                        kval = Array.IndexOf(Unique_Values, PerOutValue)
                        'Value_Colored(kval) = True
                        Exit For
                    End If
                Next

            Next
        Next

    End Sub

    Private Sub tbCurrWindInd_Return(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbCurrWindInd.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            'If rbWindow.Checked Then
            CurrWindInd = CType(tbCurrWindInd.Text, Integer)
            If CurrWindInd > NWindows Then CurrWindInd = NWindows
            If CurrWindInd < 0 Then CurrWindInd = 0


            MoveWindow(CurrWindInd)

        End If
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbResize.Click
        Dim arr() As String
        If bMapLoaded = False Then Exit Sub
        If CurrHistInd > 1 Then CurrHistInd = CurrHistInd - 1
        bWindStage = False
        Dim ZoomMap As MapInfo
        DispMap = MapHistory(CurrHistInd)
        'ResetTSButtons(False)
        If bMapLoaded = True Then
            ZoomMap.MinX = 0
            ZoomMap.MaxX = panelMap.Size.Width
            ZoomMap.MinY = 0
            ZoomMap.MaxY = panelMap.Size.Height
            arr = Split(tbCoords.Text, ",")
            DispMap.MinX = CType(arr(0), Single)
            DispMap.MaxX = CType(arr(1), Single)
            DispMap.MinY = CType(arr(2), Single)
            DispMap.MaxY = CType(arr(3), Single)

            ZoomBitmap(ZoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, False) ', CurrWindInd)
            CurrHistInd = CurrHistInd + 1

            If CurrHistInd > MaxHistory Then
                RefreshHistory(MapHistory)
                CurrHistInd = MaxHistory
            End If
            If CurrHistInd > 0 Then tsbPrev.Enabled = True
            MapHistory(CurrHistInd) = DispMap
        End If
        If CurrHistInd > 1 Then tsbPrev.Enabled = True
        If CurrHistInd <= 1 Then tsbPrev.Enabled = False
        If bSelected = True Then RedrawSelected()
        If bHoldIdentify = True Then RedrawIdentified()
        Me.Refresh()
    End Sub

    Private Sub SwitchXYToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SwitchXYToolStripMenuItem.Click

        Dim colind As Integer

        'switches the x and y values of the polygons - inverts the map


        Dim TempX() As Integer
        ReDim TempX(MxStandID)
        Dim TempMinX() As Double
        Dim TempMaxX() As Double
        ReDim TempMinX(NWindows), TempMaxX(NWindows)

        For j As Integer = 1 To MxStandID
            TempX(j) = GRIDPolys_xVal(j)
            GRIDPolys_xVal(j) = GRIDPolys_yVal(j)
            GRIDPolys_yVal(j) = TempX(j)
        Next

        For j As Integer = 1 To NWindows
            TempMinX(j) = Windows_MinX(j)
            TempMaxX(j) = Windows_MaxX(j)
            Windows_MinX(j) = Windows_MinY(j)
            Windows_MaxX(j) = Windows_MaxY(j)
            Windows_MinY(j) = TempMinX(j)
            Windows_MaxY(j) = TempMaxX(j)
        Next
        BitMapExtent(CurrWindInd) = DispMap
        BitMapExtent(CurrWindInd).MinX = DispMap.MinY
        BitMapExtent(CurrWindInd).MaxX = DispMap.MaxY
        BitMapExtent(CurrWindInd).MinY = DispMap.MinX
        BitMapExtent(CurrWindInd).MaxY = DispMap.MaxX

        'reset the fullmap
        Dim TempMap As MapInfo
        TempMap = FullMap
        FullMap.MaxX = TempMap.MaxY
        FullMap.MinX = TempMap.MinY
        FullMap.MinY = TempMap.MinX
        FullMap.MaxY = TempMap.MaxX

        Dim ZoomMap As MapInfo
        ZoomMap.MinX = 0
        ZoomMap.MaxX = panelMap.Size.Width
        ZoomMap.MinY = 0
        ZoomMap.MaxY = panelMap.Size.Height

        DispMap = BitMapExtent(CurrWindInd)
        FireEventEdgeQual = FireEventEdgeQual * -1

        'set new edge info.
        DisplayField = tsbcbbStandPoly.Text
        SelectorText = DisplayField & " ID(s) to Select"
        'determine which field you are displaying
        colind = 0
        For jatt As Integer = 1 To NumAtts
            If AttName(jatt) = DisplayField Then
                colind = jatt
                Exit For
            End If
        Next

        FillPolyDispValue(colind)

        ZoomBitmap(ZoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, False)
        If bSelected = True Then RedrawSelected()
        If bHoldIdentify = True Then RedrawIdentified()
        Me.Refresh()
    End Sub

    Private Sub tsbFireEvents_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbFireEvents.Click
        Dim OpenFileDialog1 As New OpenFileDialog
        Dim fnum As Integer
        Dim dummy As String

        With OpenFileDialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the fire event file"
            '.Filter = "Text Files(.txt) |*.txt"
            If .ShowDialog = System.Windows.Forms.DialogResult.Cancel Then Return
        End With

        FireEventFile = OpenFileDialog1.FileName
        OpenFileDialog1 = Nothing
        fnum = FreeFile()
        FileOpen(fnum, FireEventFile, OpenMode.Input)
        dummy = Trim(LineInput(fnum))
        FileClose(fnum)
        If dummy = "DETAILED FIRE EVENT REPORT" Then
            ConvertFireEventFile(FireEventFile, FireEventFile)
        End If


        LoadFireEvents(FireEventFile)
        cbFireEvents.Enabled = True
        EditFireBoundaryColorToolStripMenuItem.Enabled = True
        cbFireEvents.Checked = True

        Dim ZoomMap As MapInfo
        ZoomMap.MinX = 0
        ZoomMap.MaxX = panelMap.Size.Width
        ZoomMap.MinY = 0
        ZoomMap.MaxY = panelMap.Size.Height

        ReDim bPainted(NWindows)
        ReDim WindStageBitMap(NWindows)
        DispMap = BitMapExtent(CurrWindInd)


        MoveWindow(CurrWindInd) '        ZoomBitmap(ZoomMap, DispMap, LegendColor, bDrawEdges, bDrawFireEvents, False)

    End Sub


    Private Sub SaveThisMapAsDocumentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveThisMapAsDocumentToolStripMenuItem.Click
        If bMapLoaded = False Then
            MsgBox("Map Data needs to be loaded before you can save the map document")
            Exit Sub
        End If

        Dim fnum As Integer
        Dim filename As String
        Dim SFD1 As New SaveFileDialog
        With SFD1
            .Title = "Save the Map Document"
            .Filter = "Text Files (.mmpt) |*.mmpt"
            If MapFileFN <> "" Then
                .FileName = FName(MapFileFN)
            Else
                .FileName = "MapFileDocument_" & Windows_Label(0)
            End If

            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        filename = SFD1.FileName
        SFD1 = Nothing
        'If System.IO.File.Exists(filename) = False Then
        '    msgbox("Invalid 
        'End If
        fnum = FreeFile()
        FileOpen(fnum, filename, OpenMode.Output)

        PrintLine(fnum, MapPolyFile)
        PrintLine(fnum, OutputByPeriodFile)
        PrintLine(fnum, ColorIndexFile)
        PrintLine(fnum, GridFileType)
        PrintLine(fnum, FireEventFile)
        PrintLine(fnum, "FireEventColor," & FireEventColor.R.ToString & "," & FireEventColor.G.ToString & "," & FireEventColor.B.ToString)
        PrintLine(fnum, "FireOriginColor," & FireOriginColor.R.ToString & "," & FireOriginColor.G.ToString & "," & FireOriginColor.B.ToString)
        PrintLine(fnum, "Extent," & tbCoords.Text)
        PrintLine(fnum, "Window," & CurrWindInd)
        PrintLine(fnum, "Display Attribute," & DispAtt)

        FileClose(fnum)

    End Sub

    Private Sub LoadMapDocumentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoadMapDocumentToolStripMenuItem.Click
        Dim fnum As Integer
        'Dim filename As String
        Dim arr() As String
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        Dim OpenFileDialog1 As New OpenFileDialog
        Dim TempMap As MapInfo
        Dim TempWindInd As Integer
        With OpenFileDialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the map file"
            .Filter = "Text Files(.mmpt) |*.mmpt"
            If .ShowDialog = System.Windows.Forms.DialogResult.Cancel Then Return
        End With
        MapFileFN = OpenFileDialog1.FileName
        OpenFileDialog1 = Nothing

        'set the fireeventedgequal
        FireEventEdgeQual = 1

        bColorSaved = False
        fnum = FreeFile()
        FileOpen(fnum, MapFileFN, OpenMode.Input)

        MapPolyFile = LineInput(fnum)
        If System.IO.File.Exists(MapPolyFile) = False Then
            MsgBox("Could not find file " & MapPolyFile)
            Dim OpenFileDialog2 As New OpenFileDialog
            With OpenFileDialog2
                '.InitialDirectory = "d:\Analysis\"
                .Title = "Select the file with stand attributes, x/y coordinate (or row/column) info (e.g., .attributesall)"
                '.Filter = "Text Files(.txt) |*.txt"
                If .ShowDialog = System.Windows.Forms.DialogResult.Cancel Then Return
            End With
            MapPolyFile = OpenFileDialog2.FileName
            OpenFileDialog2 = Nothing
        End If

        OutputByPeriodFile = LineInput(fnum)
        If System.IO.File.Exists(MapPolyFile) = False Then
            MsgBox("Could not find file " & OutputByPeriodFile)
            Dim OpenFileDialog2 As New OpenFileDialog
            With OpenFileDialog2
                .Title = "Select file with stand-by-stand outputs by time period"
                .Filter = "Text Files(.txt) |*.txt"
                If .ShowDialog = System.Windows.Forms.DialogResult.Cancel Then Return
            End With
            OutputByPeriodFile = OpenFileDialog2.FileName
            OpenFileDialog2 = Nothing
        End If


        ColorIndexFile = LineInput(fnum)
        bColorFile = True
        If System.IO.File.Exists(ColorIndexFile) = False Then
            MsgBox("Could not find Color File")
            bColorFile = False
        End If

        GridFileType = LineInput(fnum)
        FireEventFile = LineInput(fnum)
        arr = Split(LineInput(fnum), ",")
        r = Trim(arr(1))
        g = Trim(arr(2))
        b = Trim(arr(3))
        FireEventColor = Color.FromArgb(255, r, g, b)
        arr = Split(LineInput(fnum), ",")
        r = Trim(arr(1))
        g = Trim(arr(2))
        b = Trim(arr(3))
        FireOriginColor = Color.FromArgb(255, r, g, b)
        arr = Split(LineInput(fnum), ",") 'min,max x and y coords
        TempMap.MinX = CType(arr(1), Single)
        TempMap.MaxX = CType(arr(2), Single)
        TempMap.MinY = CType(arr(3), Single)
        TempMap.MaxY = CType(arr(4), Single)
        arr = Split(LineInput(fnum), ",") 'window index
        TempWindInd = CType(arr(1), Integer)
        DispAtt = ""
        Do While Not EOF(fnum)
            arr = Split(LineInput(fnum), ",") 'dispatt
            DispAtt = arr(1)
            Exit Do
        Loop

        FileClose(fnum)

        lblFileName.Text = OutputByPeriodFile

        bMapLoaded = False
        AddTheme() 'this creates a default dispmap and currwindind...have to reload
        TempMap.CellWidth = DispMap.CellWidth 'got this from the add theme

        If GridFileType = "XY" Then tsbFireEvents.Enabled = True
        cbFireEvents.Checked = False 'default
        cbFireEvents.Enabled = False
        If System.IO.File.Exists(FireEventFile) = True Then
            bDrawFireEvents = True
            LoadFireEvents(FireEventFile)
            cbFireEvents.Enabled = True
            cbFireEvents.Checked = True
            EditFireBoundaryColorToolStripMenuItem.Enabled = True
        End If

        DispMap = TempMap
        CurrWindInd = TempWindInd
        MoveWindow(CurrWindInd)
        'start over on history
        CurrHistInd = 1
        If CurrHistInd > MaxHistory Then RefreshHistory(MapHistory)
        MapHistory(CurrHistInd) = DispMap
        tsbPrev.Enabled = False
        DisplayField = tsbcbbStandPoly.Text
        PopulateDispID(DisplayField)

    End Sub

    Private Sub EditCurrentColorsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditCurrentColorsToolStripMenuItem.Click
        Dim bloaded As Boolean
        bloaded = System.IO.File.Exists(ColorIndexFile)
        If bloaded = False Then
            MsgBox("Color File Not Loaded")
            Exit Sub
        End If
        bCancel = False

        Dim colorform As New Colors
        colorform.ShowDialog()
        colorform = Nothing

        If bCancel = False Then
            bColorFile = True
            LoadWindowColorInfo(ColorIndexFile)
            ReDefineMapColors()
            For jwind As Integer = 0 To NWindows
                bPainted(jwind) = False
            Next
            If bMapLoaded = True Then MoveWindow(CurrWindInd)
        End If
    End Sub

    Private Sub LoadOtherColorFileToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoadOtherColorFileToolStripMenuItem.Click

        Dim openfiledialog1 As New OpenFileDialog
        'the color index file
        With openfiledialog1
            '.InitialDirectory = "d:\Analysis\"
            .Title = "Select the file with color information"
            .Filter = "Color Files(.clr) |*.clr"
            If .ShowDialog = System.Windows.Forms.DialogResult.Cancel Then Return
        End With
        bColorFile = True
        ColorIndexFile = openfiledialog1.FileName
        openfiledialog1 = Nothing

        'If bCancel = False Then
        LoadWindowColorInfo(ColorIndexFile)
        ReDefineMapColors()
        For jwind As Integer = 0 To NWindows
            bPainted(jwind) = False
        Next
        If bMapLoaded = True Then MoveWindow(CurrWindInd)
        'End If
    End Sub

    Private Sub EditFireBoundaryColorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditFireBoundaryColorToolStripMenuItem.Click
        bCancel = False

        Dim colorform As New FireEventColors
        colorform.ShowDialog()
        colorform = Nothing

        If bCancel = False Then
            'LoadWindowColorInfo(ColorIndexFile)
            'ReDefineMapColors()
            For jwind As Integer = 0 To NWindows
                bPainted(jwind) = False
            Next
            If bMapLoaded = True Then MoveWindow(CurrWindInd)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        PlayDelayTime = PlayDelayTime * 1.25
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        PlayDelayTime = PlayDelayTime * 0.75
    End Sub

    Private Sub ListView1_Keypress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ListView1.KeyPress '.EventArgs) Handles ListView1.Click
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then UpdateColors()
    End Sub
    Private Sub ListView1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.DoubleClick '.EventArgs) Handles ListView1.Click
        UpdateColors()
    End Sub
    Private Sub UpdateColors()
        'need to get the list's index, then figure out how to reset the color.
        Dim NSelectedColors = -1
        Dim dummy As String
        If ListView1.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim ind() As Integer
        NSelectedColors = ListView1.SelectedItems.Count - 1
        ReDim ind(NSelectedColors), ChangeColors(NSelectedColors)
        For jind As Integer = 0 To NSelectedColors
            ind(jind) = ListView1.SelectedItems(jind).Index
            ChangeColors(jind) = LegendColor(ind(jind))
        Next
        'ind = ListView1.SelectedItems '(0).Index
        'save this new color to the .clr file
        'ChangeColor = LegendColor(ind)

        Dim cpicker As New ColorPicker
        cpicker.ShowDialog()
        cpicker = Nothing
        If bColorSaved = True Then
            For jind As Integer = 0 To NSelectedColors
                LegendColor(ind(jind)) = ChangeColors(jind)
            Next
            Dim fnum As Integer
            fnum = FreeFile()
            'read in the lines that are there now
            If System.IO.File.Exists(ColorIndexFile) = False Then
                Dim SFD1 As New SaveFileDialog
                With SFD1
                    .Title = "Save the color file"
                    .Filter = "Text Files (.clr) |*.clr"
                    .FileName = FName(OutputByPeriodFile)

                    If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
                End With
                ColorIndexFile = SFD1.FileName
                bColorFile = True
                SFD1 = Nothing
            End If

            FileOpen(fnum, ColorIndexFile, OpenMode.Output)
            PrintLine(fnum, "NColors")
            PrintLine(fnum, NColors)
            PrintLine(fnum, "LabelColorIndex(i),ColmnName(s),LabelString(s),LabelMinVal(i),LabelMaxVal(i),R,G,B")
            For jline As Integer = 1 To NColors  'rtbColorFile.Lines.Length - 1
                dummy = LegendColor(jline).LabelColorInd & "," & Windows_Label(0) & "," & LegendColor(jline).LabelDispStr & "," & Math.Round(LegendColor(jline).LabelMinVal, 2) & "," & Math.Round(LegendColor(jline).LabelMaxVal, 2)
                dummy = dummy & "," & LegendColor(jline).Color.R & "," & LegendColor(jline).Color.G & "," & LegendColor(jline).Color.B
                PrintLine(fnum, dummy)
            Next
            FileClose(fnum)

            LoadWindowColorInfo(ColorIndexFile)
            ReDefineMapColors()
            For jwind As Integer = 0 To NWindows
                bPainted(jwind) = False
            Next
            If bMapLoaded = True Then MoveWindow(CurrWindInd)
        End If
        ListView1.SelectedItems.Clear()
    End Sub


    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem.Click
        MsgBox("This version should allow you to define colors from scratch without a predefined color file.")

    End Sub



    Private Sub tsbcbbStandPoly_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbcbbStandPoly.SelectedIndexChanged
        DisplayField = tsbcbbStandPoly.Text
        PopulateDispID(DisplayField)
    End Sub

    Private Sub DefineNewColorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DefineNewColorToolStripMenuItem.Click
        If bMapLoaded = False Then
            MsgBox("No data loaded")
            Exit Sub
        End If
        Dim dummy As String
        Dim ind As Integer
        'save this new color to the .clr file
        ReDim ChangeColors(0)
        ChangeColors(0) = LegendColor(0)

        Dim cpicker As New ColorPicker
        cpicker.ShowDialog()
        cpicker = Nothing
        If bColorSaved = True Then
            ind = NColors + 1
            NColors = ind
            ReDim Preserve LegendColor(ind)
            LegendColor(ind) = ChangeColors(0)
            LegendColor(ind).LabelColorInd = NColors
            Dim fnum As Integer
            fnum = FreeFile()
            'read in the lines that are there now
            If System.IO.File.Exists(ColorIndexFile) = False Then
                Dim SFD1 As New SaveFileDialog
                With SFD1
                    .Title = "Save the color file"
                    .Filter = "Text Files (.clr) |*.clr"
                    .FileName = FName(OutputByPeriodFile)

                    If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
                End With
                ColorIndexFile = SFD1.FileName
                bColorFile = True
                SFD1 = Nothing
            End If

            FileOpen(fnum, ColorIndexFile, OpenMode.Output)
            PrintLine(fnum, "NColors")
            PrintLine(fnum, NColors)
            PrintLine(fnum, "LabelColorIndex(i),ColmnName(s),LabelString(s),LabelMinVal(i),LabelMaxVal(i),R,G,B")
            For jline As Integer = 1 To NColors  'rtbColorFile.Lines.Length - 1
                dummy = LegendColor(jline).LabelColorInd & "," & Windows_Label(0) & "," & LegendColor(jline).LabelDispStr & "," & Math.Round(LegendColor(jline).LabelMinVal, 2) & "," & Math.Round(LegendColor(jline).LabelMaxVal, 2)
                dummy = dummy & "," & LegendColor(jline).Color.R & "," & LegendColor(jline).Color.G & "," & LegendColor(jline).Color.B
                PrintLine(fnum, dummy)
            Next
            FileClose(fnum)

            LoadWindowColorInfo(ColorIndexFile)
            ReDefineMapColors()
            For jwind As Integer = 0 To NWindows
                bPainted(jwind) = False
            Next
            If bMapLoaded = True Then MoveWindow(CurrWindInd)
        End If
        ListView1.SelectedItems.Clear()
    End Sub

    Public Sub PopulateDispID(ByVal sp As String)
        Dim colind As Integer
        'StandPoly = tsbcbbStandPoly.Text
        'electorText = sp & " ID(s) to Select"
        'determine which field you are displaying
        colind = 0
        For jatt As Integer = 1 To NumAtts
            If AttName(jatt) = sp Then
                colind = jatt
                Exit For
            End If
        Next
        FillPolyDispValue(colind)
    End Sub
    Public Sub PopulateQueryID(ByVal sp As String)
        Dim colind As Integer
        'StandPoly = tsbcbbStandPoly.Text
        SelectorText = sp & " ID(s) to Select"
        'determine which field you are displaying
        colind = 0
        For jatt As Integer = 1 To NumAtts
            If AttName(jatt) = sp Then
                colind = jatt
                Exit For
            End If
        Next
        FillPolyQueryValue(colind)
    End Sub
    Public Sub PopulateIdentifyID(ByVal sp As String)
        Dim colind As Integer
        'StandPoly = tsbcbbStandPoly.Text
        'SelectorText = sp & " ID(s) to Select"
        'determine which field you are displaying
        colind = 0
        For jatt As Integer = 1 To NumAtts
            If AttName(jatt) = sp Then
                colind = jatt
                Exit For
            End If
        Next
        FillPolyIdentifyValue(colind)
    End Sub

    Private Sub AddAllColorsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddAllColorsToolStripMenuItem.Click
        If bMapLoaded = False Then
            MsgBox("No data loaded")
            Exit Sub
        End If
        Dim dummy As String
        Dim ind As Integer
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        'save this new color to the .clr file
        ReDim ChangeColors(0)
        ChangeColors(0) = LegendColor(0)

        'Dim cpicker As New ColorPicker
        'cpicker.ShowDialog()
        'cpicker = Nothing
        NColors = NUniqueValues
        ReDim LegendColor(NColors)
        For jcolor As Integer = 1 To NUniqueValues
            r = Rnd() * 255
            g = Rnd() * 255
            b = Rnd() * 255
            LegendColor(jcolor).Color = Color.FromArgb(255, r, g, b)
            LegendColor(jcolor).LabelColorInd = jcolor
            'Windows_Label(jcolor) = "INDEX"
            LegendColor(jcolor).LabelDispStr = Unique_Values(jcolor)
            LegendColor(jcolor).LabelMinVal = Unique_Values(jcolor)
            LegendColor(jcolor).LabelMaxVal = Unique_Values(jcolor)

        Next
        'If bColorSaved = True Then
        ind = NColors + 1
        NColors = ind
        ReDim Preserve LegendColor(ind)
        LegendColor(ind) = ChangeColors(0)
        LegendColor(ind).LabelColorInd = NColors
        Dim fnum As Integer
        fnum = FreeFile()
        'read in the lines that are there now
        If System.IO.File.Exists(ColorIndexFile) = False Then
            Dim SFD1 As New SaveFileDialog
            With SFD1
                .Title = "Save the color file"
                .Filter = "Text Files (.clr) |*.clr"
                .FileName = FName(OutputByPeriodFile)

                If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
            End With
            ColorIndexFile = SFD1.FileName
            bColorFile = True
            SFD1 = Nothing
        End If

        FileOpen(fnum, ColorIndexFile, OpenMode.Output)
        PrintLine(fnum, "NColors")
        PrintLine(fnum, NColors)
        PrintLine(fnum, "LabelColorIndex(i),ColmnName(s),LabelString(s),LabelMinVal(i),LabelMaxVal(i),R,G,B")
        For jline As Integer = 1 To NColors  'rtbColorFile.Lines.Length - 1
            dummy = LegendColor(jline).LabelColorInd & "," & Windows_Label(0) & "," & LegendColor(jline).LabelDispStr & "," & Math.Round(LegendColor(jline).LabelMinVal, 2) & "," & Math.Round(LegendColor(jline).LabelMaxVal, 2)
            dummy = dummy & "," & LegendColor(jline).Color.R & "," & LegendColor(jline).Color.G & "," & LegendColor(jline).Color.B
            PrintLine(fnum, dummy)
        Next
        FileClose(fnum)

        LoadWindowColorInfo(ColorIndexFile)
        ReDefineMapColors()
        For jwind As Integer = 0 To NWindows
            bPainted(jwind) = False
        Next
        If bMapLoaded = True Then MoveWindow(CurrWindInd)
        'End If
        ListView1.SelectedItems.Clear()
    End Sub

    Private Sub SaveLoadedImagesToJPGToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveLoadedImagesToJPGToolStripMenuItem.Click
        If bMapLoaded = False Then
            MsgBox("No data loaded")
            Exit Sub
        End If

        Dim fnum As Integer
        Dim filename As String
        Dim jpgfilename As String
        Dim SFD1 As New SaveFileDialog
        'need a Save-As dialog
        With SFD1
            .Title = "Save the Map Images to JPG"
            .Filter = "Text Files (.jpg) |*.jpg"
            If MapFileFN <> "" Then
                .FileName = FName(MapFileFN)
            Else
                .FileName = "MapFileDocument_" & Windows_Label(0)
            End If

            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        filename = SFD1.FileName
        SFD1 = Nothing

        'Then loop through all of the loaded maps and see if they are painted. If so, save the WindStageBitMap(CurrWindInd)

        For jwind As Integer = 0 To NWindows
            If bPainted(jwind) = True Then
                jpgfilename = System.IO.Path.GetDirectoryName(filename) & "\" & FName(filename) & "_" & jwind & ".jpg"
                'WindStageBitMap(jwind).Save(jpgfilename, Imaging.ImageFormat.Jpeg)
                FullProgramBitMap(jwind).Save(jpgfilename, Imaging.ImageFormat.Jpeg)
            End If
        Next

        MsgBox("Files saved to: " & filename)
    End Sub

    Private Sub SaveThisMapToJPGToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveThisMapToJPGToolStripMenuItem.Click
        If bMapLoaded = False Then
            MsgBox("No data loaded")
            Exit Sub
        End If

        Dim fnum As Integer
        Dim filename As String
        Dim jpgfilename As String
        Dim SFD1 As New SaveFileDialog
        'need a Save-As dialog
        With SFD1
            .Title = "Save the Map Image to JPG"
            .Filter = "Text Files (.jpg) |*.jpg"
            If MapFileFN <> "" Then
                .FileName = FName(MapFileFN)
            Else
                .FileName = "MapFileDocument_" & Windows_Label(0)
            End If

            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        filename = SFD1.FileName
        SFD1 = Nothing

        'Then loop through all of the loaded maps and see if they are painted. If so, save the WindStageBitMap(CurrWindInd)

        'For jwind As Integer = 0 To NWindows
        If bPainted(CurrWindInd) = True Then
            jpgfilename = System.IO.Path.GetDirectoryName(filename) & "\" & FName(filename) & "_" & CurrWindInd & ".jpg"
            'WindStageBitMap(jwind).Save(jpgfilename, Imaging.ImageFormat.Jpeg)
            FullProgramBitMap(CurrWindInd).Save(jpgfilename, Imaging.ImageFormat.Jpeg)
        End If
        'Next

        MsgBox("Files saved to: " & filename)
    End Sub


    Private Sub SaveThisImage(ByVal format As String)
        If bMapLoaded = False Then
            MsgBox("No data loaded")
            Exit Sub
        End If

        Dim grp As System.Drawing.Graphics
        Dim CropImage As System.Drawing.Bitmap
        Dim MapWidth As Double
        Dim MapHeight As Double
        Dim filename As String
        Dim imgfilename As String

        Dim SFD1 As New SaveFileDialog
        Dim filtertext As String

        filtertext = "Image Files (." & format & ") |*." & format
        With SFD1
            .Title = "Save the Map Image to " & format  'TIF"
            .Filter = filtertext '"Text Files (.tif) |*.tif"
            If MapFileFN <> "" Then
                .FileName = FName(MapFileFN)
            Else
                .FileName = "MapFileDocument_" & Windows_Label(0)
            End If

            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        filename = SFD1.FileName
        SFD1 = Nothing

        'Then loop through all of the loaded maps and see if they are painted. If so, save the WindStageBitMap(CurrWindInd)

        'For jwind As Integer = 0 To NWindows
        If bPainted(CurrWindInd) = True Then
            imgfilename = System.IO.Path.GetDirectoryName(filename) & "\" & FName(filename) & "_" & CurrWindInd & "." & format

            MapWidth = Math.Round((WindStageBitMap(CurrWindInd).Width * DispMap.xScale), 0)
            MapHeight = Math.Round((WindStageBitMap(CurrWindInd).Height * DispMap.yScale), 0)

            Dim CropRect As New Rectangle(0, 0, MapWidth, MapHeight)
            CropImage = New Bitmap(CropRect.Width, CropRect.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
            grp = Graphics.FromImage(CropImage)
            grp.DrawImage(WindStageBitMap(CurrWindInd), New Rectangle(0, 0, CropRect.Width, CropRect.Height), CropRect, GraphicsUnit.Pixel)
            'WindStageBitMap(jwind).Save(jpgfilename, Imaging.ImageFormat.Jpeg)
            If format = "tif" Then CropImage.Save(imgfilename, Imaging.ImageFormat.Tiff)
            If format = "jpg" Then CropImage.Save(imgfilename, Imaging.ImageFormat.Jpeg)
            If format = "bmp" Then CropImage.Save(imgfilename, Imaging.ImageFormat.Bmp)
            If format = "gif" Then CropImage.Save(imgfilename, Imaging.ImageFormat.Gif)
            MsgBox("Files saved to: " & filename)
        Else
            MsgBox("File not saved.")
        End If


    End Sub

    Private Sub RedrawSelected()
        'search for the Stand ID  of the poly with the center closest to the clicked point on the map
        Dim selarea As Double = 0
        QueryMapSpace(DispMap, iSelID, selarea)
        bFlashSel = True
        Label6.Text = QueryField & " ID: " & iSelID(1)
        Label2.Text = "Area: " & Math.Round(selarea, 0)
    End Sub
    Private Sub RedrawIdentified()

        If IdentifyField = "" Then
            IdentifyField = DisplayField
            PopulateIdentifyID(IdentifyField)
        End If
        'search for the Stand ID and number of prescriptions of the poly with the center closest to the clicked point on the map
        Dim idarea As Double
        IdentifyMapSpace(DispMap, iIdnID, idarea)

    End Sub

    Private Sub TIFToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TIFToolStripMenuItem.Click
        SaveThisImage("tif")
    End Sub

    Private Sub JPEGToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JPEGToolStripMenuItem.Click
        SaveThisImage("jpg")
    End Sub

    Private Sub BITMAPToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BITMAPToolStripMenuItem.Click
        SaveThisImage("bmp")
    End Sub

    Private Sub GIFToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GIFToolStripMenuItem.Click
        SaveThisImage("gif")
    End Sub

    Private Sub tsbcbbStandPoly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbcbbStandPoly.Click

    End Sub
End Class