<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSIMPPLLE_Results
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSubmit = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.tbSIMPPLLEOutputFolder = New System.Windows.Forms.TextBox
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.cbbRunToDisplay = New System.Windows.Forms.ComboBox
        Me.cbbAttToDisplay = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnCheck = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'btnSubmit
        '
        Me.btnSubmit.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubmit.Location = New System.Drawing.Point(99, 222)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.Size = New System.Drawing.Size(89, 32)
        Me.btnSubmit.TabIndex = 0
        Me.btnSubmit.Text = "Submit"
        Me.btnSubmit.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(217, 222)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(89, 32)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(211, 20)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "SIMPPLLE Output Folder"
        '
        'tbSIMPPLLEOutputFolder
        '
        Me.tbSIMPPLLEOutputFolder.Location = New System.Drawing.Point(11, 36)
        Me.tbSIMPPLLEOutputFolder.Name = "tbSIMPPLLEOutputFolder"
        Me.tbSIMPPLLEOutputFolder.Size = New System.Drawing.Size(325, 20)
        Me.tbSIMPPLLEOutputFolder.TabIndex = 3
        Me.tbSIMPPLLEOutputFolder.Text = "<Output Folder ""-simulation"">"
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(349, 22)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(59, 20)
        Me.btnBrowse.TabIndex = 4
        Me.btnBrowse.Text = "Browse"
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(182, 20)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Select Run to Display"
        '
        'cbbRunToDisplay
        '
        Me.cbbRunToDisplay.Enabled = False
        Me.cbbRunToDisplay.FormattingEnabled = True
        Me.cbbRunToDisplay.Location = New System.Drawing.Point(11, 102)
        Me.cbbRunToDisplay.Name = "cbbRunToDisplay"
        Me.cbbRunToDisplay.Size = New System.Drawing.Size(142, 21)
        Me.cbbRunToDisplay.TabIndex = 6
        '
        'cbbAttToDisplay
        '
        Me.cbbAttToDisplay.Enabled = False
        Me.cbbAttToDisplay.FormattingEnabled = True
        Me.cbbAttToDisplay.Location = New System.Drawing.Point(11, 167)
        Me.cbbAttToDisplay.Name = "cbbAttToDisplay"
        Me.cbbAttToDisplay.Size = New System.Drawing.Size(142, 21)
        Me.cbbAttToDisplay.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 140)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(219, 20)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Select Attribute to Display"
        '
        'btnCheck
        '
        Me.btnCheck.Location = New System.Drawing.Point(349, 48)
        Me.btnCheck.Name = "btnCheck"
        Me.btnCheck.Size = New System.Drawing.Size(59, 20)
        Me.btnCheck.TabIndex = 9
        Me.btnCheck.Text = "Check"
        Me.btnCheck.UseVisualStyleBackColor = True
        '
        'frmSIMPPLLE_Results
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(413, 262)
        Me.Controls.Add(Me.btnCheck)
        Me.Controls.Add(Me.cbbAttToDisplay)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cbbRunToDisplay)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.tbSIMPPLLEOutputFolder)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSubmit)
        Me.Name = "frmSIMPPLLE_Results"
        Me.Text = "SIMPPLLE Results To Display"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSubmit As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbSIMPPLLEOutputFolder As System.Windows.Forms.TextBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbbRunToDisplay As System.Windows.Forms.ComboBox
    Friend WithEvents cbbAttToDisplay As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnCheck As System.Windows.Forms.Button
End Class
