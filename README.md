# MMaPPit #

OpenSIMPPLLE Output visualization

### What is this repository for? ###

* Display maps of queries processed through SOAP
* NEW: Direct display of OpenSIMPPLLE outputs (version 1.3.9 or later) - no SOAP process required
* Latest version 4/28/2017

### How do I get set up? ###

* Download the latest .exe from the Downloads page
* Save to any folder
* Click .exe in folder to open
* Use "add theme" button in toolbar or "Load Map File" from menu

### Key Updates ###
* New for 4/28/2017 version - click the OpenSIMPPLLE icon in the toolbar and directly navigate to a run folder to display results. Works for OpenSIMPPLLE 1.3.9 or later
* 4/28/2017 - select multiple colors to change at a time - double click to access color dialog.


### Contact ###

* Eric Henderson
* ehenderson@fs.fed.us