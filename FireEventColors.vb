Public Class FireEventColors
    Dim bSaved As Boolean

    Private Sub FireEventColors_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bSaved = False
        tbFEr.Text = FireEventColor.R.ToString
        tbFEg.Text = FireEventColor.G.ToString
        tbFEb.Text = FireEventColor.B.ToString
        pbFE.BackColor = FireEventColor

        tbFOr.Text = FireOriginColor.R.ToString
        tbFOg.Text = FireOriginColor.G.ToString
        tbFOb.Text = FireOriginColor.B.ToString
        pbFO.BackColor = FireOriginColor
    End Sub

    Private Sub tbFEr_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFEr.Leave, tbFEr.Enter
        ' Dim arr As Integer
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        r = tbFEr.Text
        If r < 0 Then r = 0
        If r > 255 Then r = 255
        tbFEr.Text = r
        g = tbFEg.Text
        If g < 0 Then g = 0
        If g > 255 Then g = 255
        tbFEg.Text = g
        b = tbFEb.Text
        If b < 0 Then b = 0
        If b > 255 Then b = 255
        tbFEb.Text = b
        FireEventColor = Color.FromArgb(255, r, g, b)
        pbFE.BackColor = FireEventColor
    End Sub

    Private Sub tbFEg_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFEg.Leave, tbFEg.Enter
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        r = tbFEr.Text
        If r < 0 Then r = 0
        If r > 255 Then r = 255
        tbFEr.Text = r
        g = tbFEg.Text
        If g < 0 Then g = 0
        If g > 255 Then g = 255
        tbFEg.Text = g
        b = tbFEb.Text
        If b < 0 Then b = 0
        If b > 255 Then b = 255
        tbFEb.Text = b
        FireEventColor = Color.FromArgb(255, r, g, b)
        pbFE.BackColor = FireEventColor
    End Sub

    Private Sub tbFEb_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFEb.Leave, tbFEb.Enter
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        r = tbFEr.Text
        If r < 0 Then r = 0
        If r > 255 Then r = 255
        tbFEr.Text = r
        g = tbFEg.Text
        If g < 0 Then g = 0
        If g > 255 Then g = 255
        tbFEg.Text = g
        b = tbFEb.Text
        If b < 0 Then b = 0
        If b > 255 Then b = 255
        tbFEb.Text = b
        FireEventColor = Color.FromArgb(255, r, g, b)
        pbFE.BackColor = FireEventColor
    End Sub




    Private Sub tbFOr_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFOr.Leave, tbFOr.Enter
        ' Dim arr As Integer
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        r = tbFOr.Text
        If r < 0 Then r = 0
        If r > 255 Then r = 255
        tbFOr.Text = r
        g = tbFOg.Text
        If g < 0 Then g = 0
        If g > 255 Then g = 255
        tbFOg.Text = g
        b = tbFOb.Text
        If b < 0 Then b = 0
        If b > 255 Then b = 255
        tbFOb.Text = b
        FireOriginColor = Color.FromArgb(255, r, g, b)
        pbFO.BackColor = FireOriginColor
    End Sub

    Private Sub tbFog_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFOg.Leave, tbFOg.Enter
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        r = tbFOr.Text
        If r < 0 Then r = 0
        If r > 255 Then r = 255
        tbFOr.Text = r
        g = tbFOg.Text
        If g < 0 Then g = 0
        If g > 255 Then g = 255
        tbFOg.Text = g
        b = tbFOb.Text
        If b < 0 Then b = 0
        If b > 255 Then b = 255
        tbFOb.Text = b
        FireOriginColor = Color.FromArgb(255, r, g, b)
        pbFO.BackColor = FireOriginColor
    End Sub

    Private Sub tbFob_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFOb.Leave, tbFOb.Enter
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        r = tbFOr.Text
        If r < 0 Then r = 0
        If r > 255 Then r = 255
        tbFOr.Text = r
        g = tbFOg.Text
        If g < 0 Then g = 0
        If g > 255 Then g = 255
        tbFOg.Text = g
        b = tbFOb.Text
        If b < 0 Then b = 0
        If b > 255 Then b = 255
        tbFOb.Text = b
        FireOriginColor = Color.FromArgb(255, r, g, b)
        pbFO.BackColor = FireOriginColor
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        bCancel = True
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        bCancel = False
        bSaved = True
        Me.Close()
    End Sub
    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If bSaved = False Then bCancel = True
    End Sub
End Class