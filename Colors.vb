Public Class Colors
    Dim bSaved As Boolean
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim MyDialog As New ColorDialog
        MyDialog.ShowDialog()
        MyDialog = Nothing
    End Sub

    Private Sub Colors_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bSaved = False
        'ColorIndexFile = "E:\NPC\SIMPPLLE\SIMPPLLE_ANALYSIS\Maps\MapColorsDFCVeg.txt"
        Dim dummy As String
        Dim fnum As Integer
        Dim Lines() As String
        Dim Nlines As Integer
        fnum = FreeFile()
        FileOpen(fnum, ColorIndexFile, OpenMode.Input)
        Do Until EOF(fnum)
            dummy = LineInput(fnum)
            If Len(Trim(dummy)) = 0 Then Exit Do
            Nlines = Nlines + 1
        Loop
        FileClose(fnum)

        ReDim Lines(Nlines) ', rtbColorFile.Lines(Nlines)
        fnum = FreeFile()
        FileOpen(fnum, ColorIndexFile, OpenMode.Input)
        For jline As Integer = 1 To Nlines
            Lines(jline) = LineInput(fnum)
            rtbColorFile.Text = rtbColorFile.Text & Lines(jline) & Chr(13)
        Next
        FileClose(fnum)


    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim berror As Boolean
        Dim fnum As Integer
        Dim arr() As String
        Dim kline As Integer

        berror = False
        fnum = FreeFile()
        FileOpen(fnum, ColorIndexFile, OpenMode.Output)
        For jline As Integer = 0 To rtbColorFile.Lines.Length - 1
            If jline > 2 Then
                arr = Split(rtbColorFile.Lines(jline), ",")
                If arr.Length <> 8 And Trim(rtbColorFile.Lines(jline)).Length > 0 Then
                    berror = True
                    kline = jline + 1
                End If
            End If
            PrintLine(fnum, rtbColorFile.Lines(jline))
        Next
        FileClose(fnum)
        If berror = True Then
            MsgBox("Error in line " & kline)
            Exit Sub 'don't close the dialog box!
        End If
        bCancel = False
        bSaved = True
        Me.Close()
    End Sub

    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If bSaved = False Then bCancel = True
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        bCancel = True
        Me.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'saves a new color file with your custom colors if you want...
        Dim savefiledialog1 As New SaveFileDialog
        With savefiledialog1
            .Title = "Save a new color File as"
            .Filter = "Color Files (.clr) |*.clr"
            .FileName = ColorIndexFile
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        ColorIndexFile = savefiledialog1.FileName
        savefiledialog1 = Nothing

        Dim fnum As Integer

        fnum = FreeFile()
        FileOpen(fnum, ColorIndexFile, OpenMode.Output)
        For jline As Integer = 0 To rtbColorFile.Lines.Length - 1
            PrintLine(fnum, rtbColorFile.Lines(jline))
        Next
        FileClose(fnum)
        bCancel = False
        bSaved = True
        savefiledialog1 = Nothing
        Me.Close()
    End Sub
End Class