Public Class frmSIMPPLLE_Results
    Dim OutputFolder As String
    Dim NSimulations As Integer
    Dim Simulations() As String
    Dim NDispAtts As Integer
    Dim DispAtts() As String
    Dim bgoodfldr As Boolean

    Private Sub frmSIMPPLLE_Results_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bCancel = True
        NDispAtts = 7
        ReDim DispAtts(NDispAtts)
        DispAtts(1) = "LIFEFORM"
        DispAtts(2) = "SPECIES"
        DispAtts(3) = "SIZECLASS"
        DispAtts(4) = "AGE"
        DispAtts(5) = "DENSITY"
        DispAtts(6) = "PROCESS"
        DispAtts(7) = "TREATMENT"
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        bCancel = True
        bgoodfldr = False

        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If bgoodfldr = False Then
            MsgBox("Cannot Display these results. Choose a valid folder, run and attribute and try again.")
            Exit Sub
        End If
        'return the selections
        GridFileType = "SIMPPLLE"
        DispAtt = cbbAttToDisplay.Text
        If DispAtt <> "AGE" Then DispAtt = cbbAttToDisplay.Text & "_ID"
        OutputByPeriodFile = tbSIMPPLLEOutputFolder.Text & "\textdata\" & cbbRunToDisplay.Text
        MapPolyFile = tbSIMPPLLEOutputFolder.Text & "\textdata\SLINKMETRICS.csv"

        bCancel = False
        Me.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        'check button


        OutputFolder = tbSIMPPLLEOutputFolder.Text & "\textdata"
        CheckFolder(OutputFolder, bgoodfldr)
        If bgoodfldr = False Then
            MsgBox("This folder does not contain OpenSIMPPLLE run outputs from version 1.3.9 or later. Cannot display these results.")
            Exit Sub
        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click

        'folderbrowser
        Dim folderbrowser1 As New FolderBrowserDialog
        With folderbrowser1
            .Description = "Folder with OpenSIMPPLLE Run Outputs from version 1.3.9 or later. Should be labeled as '-simulation' "
            If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return
        End With
        OutputFolder = folderbrowser1.SelectedPath & "\textdata"
        CheckFolder(OutputFolder, bgoodfldr)
        If bgoodfldr = False Then
            folderbrowser1 = Nothing
            MsgBox("This folder does not contain OpenSIMPPLLE run outputs from version 1.3.9 or later. Cannot display these results.")
            Exit Sub
        End If
        tbSIMPPLLEOutputFolder.Text = folderbrowser1.SelectedPath
        folderbrowser1 = Nothing

    End Sub
    Private Sub CheckFolder(ByVal outfolder As String, ByRef bGoodFolder As Boolean)
        bGoodFolder = True
        cbbAttToDisplay.Enabled = False
        cbbAttToDisplay.Text = ""
        cbbRunToDisplay.Enabled = False
        cbbRunToDisplay.Text = ""

        If System.IO.Directory.Exists(outfolder) = False Then
            bGoodFolder = False
            Return
        End If

        Dim jsim As Integer

        Dim fldr As New System.IO.DirectoryInfo(outfolder)
        Dim files As System.IO.FileInfo() = fldr.GetFiles("*EVU_SIM_DATA*")

        Dim fi As System.IO.FileInfo
        If files.Length < 1 Then
            bGoodFolder = False
            Exit Sub
        Else
            jsim = 0
            For Each fi In files
                If fi.Extension = ".csv" Then
                    jsim += 1
                End If
            Next
            If jsim = 0 Then
                bGoodFolder = False
                Exit Sub
            End If

            NSimulations = jsim
            ReDim Simulations(NSimulations)
            jsim = 0
            For Each fi In files
                If fi.Extension = ".csv" Then
                    jsim += 1
                    Simulations(jsim) = fi.Name
                End If
            Next

            'now check for SLINKMETRICS
            files = fldr.GetFiles("*SLINKMETRICS*")
            If files.Length < 1 Then
                bGoodFolder = False
                Exit Sub
            End If
        End If

        PopComboBoxes()

    End Sub
    Private Sub PopComboBoxes()
        Dim ItemArray As New ArrayList
        For j As Integer = 1 To NDispAtts
            ItemArray.Add(DispAtts(j))
        Next
        cbbAttToDisplay.Enabled = True
        cbbAttToDisplay.Items.Clear()
        cbbAttToDisplay.Items.AddRange(ItemArray.ToArray)
        cbbAttToDisplay.Text = DispAtts(1)

        ItemArray.Clear()

        For j As Integer = 1 To NSimulations
            ItemArray.Add(Simulations(j))
        Next
        cbbRunToDisplay.Enabled = True
        cbbRunToDisplay.Items.Clear()
        cbbRunToDisplay.Items.AddRange(ItemArray.ToArray)
        cbbRunToDisplay.Text = Simulations(1)

    End Sub

    Private Sub cbbAttToDisplay_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbbAttToDisplay.SelectedIndexChanged

    End Sub
End Class