Public Class frmStandAtts
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListViewAtts As System.Windows.Forms.ListView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbbAttributeField As System.Windows.Forms.ComboBox
    Friend WithEvents cbHoldIdentify As System.Windows.Forms.CheckBox
    Friend WithEvents lblOpacity As System.Windows.Forms.Label
    Friend WithEvents Opacity As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents lblStandID As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.lblStandID = New System.Windows.Forms.Label
        Me.ListViewAtts = New System.Windows.Forms.ListView
        Me.Label2 = New System.Windows.Forms.Label
        Me.cbbAttributeField = New System.Windows.Forms.ComboBox
        Me.cbHoldIdentify = New System.Windows.Forms.CheckBox
        Me.lblOpacity = New System.Windows.Forms.Label
        Me.Opacity = New System.Windows.Forms.Label
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Cooper Black", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(232, 22)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Attributes For Stand"
        '
        'Button1
        '
        Me.Button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Button1.Font = New System.Drawing.Font("Cooper Black", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(98, 611)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(74, 40)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Close"
        '
        'lblStandID
        '
        Me.lblStandID.Font = New System.Drawing.Font("Cooper Black", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStandID.Location = New System.Drawing.Point(8, 30)
        Me.lblStandID.Name = "lblStandID"
        Me.lblStandID.Size = New System.Drawing.Size(218, 22)
        Me.lblStandID.TabIndex = 5
        '
        'ListViewAtts
        '
        Me.ListViewAtts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListViewAtts.Location = New System.Drawing.Point(12, 95)
        Me.ListViewAtts.Name = "ListViewAtts"
        Me.ListViewAtts.Size = New System.Drawing.Size(256, 510)
        Me.ListViewAtts.TabIndex = 17
        Me.ListViewAtts.UseCompatibleStateImageBehavior = False
        Me.ListViewAtts.View = System.Windows.Forms.View.Details
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(4, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 16)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Attribute Field"
        '
        'cbbAttributeField
        '
        Me.cbbAttributeField.FormattingEnabled = True
        Me.cbbAttributeField.Location = New System.Drawing.Point(98, 51)
        Me.cbbAttributeField.Name = "cbbAttributeField"
        Me.cbbAttributeField.Size = New System.Drawing.Size(170, 21)
        Me.cbbAttributeField.TabIndex = 18
        '
        'cbHoldIdentify
        '
        Me.cbHoldIdentify.AutoSize = True
        Me.cbHoldIdentify.Location = New System.Drawing.Point(15, 75)
        Me.cbHoldIdentify.Name = "cbHoldIdentify"
        Me.cbHoldIdentify.Size = New System.Drawing.Size(122, 17)
        Me.cbHoldIdentify.TabIndex = 20
        Me.cbHoldIdentify.Text = "Hold Identify Display"
        Me.cbHoldIdentify.UseVisualStyleBackColor = True
        '
        'lblOpacity
        '
        Me.lblOpacity.AutoSize = True
        Me.lblOpacity.Location = New System.Drawing.Point(250, 11)
        Me.lblOpacity.Name = "lblOpacity"
        Me.lblOpacity.Size = New System.Drawing.Size(25, 13)
        Me.lblOpacity.TabIndex = 50
        Me.lblOpacity.Text = "100"
        '
        'Opacity
        '
        Me.Opacity.AutoSize = True
        Me.Opacity.Location = New System.Drawing.Point(204, 11)
        Me.Opacity.Name = "Opacity"
        Me.Opacity.Size = New System.Drawing.Size(43, 13)
        Me.Opacity.TabIndex = 49
        Me.Opacity.Text = "Opacity"
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button6.Location = New System.Drawing.Point(253, 27)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(15, 18)
        Me.Button6.TabIndex = 48
        Me.Button6.Text = "^"
        Me.Button6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(50, Byte))
        Me.Button7.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button7.Location = New System.Drawing.Point(232, 27)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(15, 18)
        Me.Button7.TabIndex = 47
        Me.Button7.Text = "v"
        Me.Button7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button7.UseVisualStyleBackColor = True
        '
        'frmStandAtts
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(291, 652)
        Me.Controls.Add(Me.lblOpacity)
        Me.Controls.Add(Me.Opacity)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.cbHoldIdentify)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbbAttributeField)
        Me.Controls.Add(Me.ListViewAtts)
        Me.Controls.Add(Me.lblStandID)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmStandAtts"
        Me.Text = "Stand Attributes"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub Selector_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim i As Integer
        Dim str As String = ""

        bHoldIdentify = False
        cbHoldIdentify.Checked = False
        If IdentifyOpacity <> 0 Then lblOpacity.Text = IdentifyOpacity
        IdentifyOpacity = CType(lblOpacity.Text, Integer)
        ListViewAtts.BeginUpdate()
        ListViewAtts.Clear()

        If IdentifyField = "" Then
            IdentifyField = DisplayField
            MMaPPit.PopulateIdentifyID(IdentifyField)
        End If


        lblStandID.Text = gStandID
        ListViewAtts.Columns.Add("Attribute Name", 90, HorizontalAlignment.Left)
        ListViewAtts.Columns.Add("Value", 120, HorizontalAlignment.Left)
        For jatt As Integer = 1 To NumAtts
            cbbAttributeField.Items.Add(AttName(jatt))
        Next
        cbbAttributeField.Text = QueryField
        For i = 1 To NumAtts
            Dim item As New ListViewItem(AttName(i), 0)
            ListViewAtts.Items.Add(item)
            item.SubItems.Add(AttName(i))

        Next i
        ListViewAtts.EndUpdate()
        bIDClosed = False

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        bIDClosed = True
        Me.Close()
    End Sub

    Public Sub UpdateID()
        Dim i As Integer
        Dim str As String = ""

        ListViewAtts.BeginUpdate()
        ListViewAtts.Clear()

        lblStandID.Text = gStandID
        ListViewAtts.Columns.Add("Attribute Name", 90, HorizontalAlignment.Left)
        ListViewAtts.Columns.Add("Value", 120, HorizontalAlignment.Left)
        For i = 1 To NumAtts
            Dim item As New ListViewItem(AttName(i), 0)
            ListViewAtts.Items.Add(item)
            item.SubItems.Add(Atts(i))

        Next i
        ListViewAtts.EndUpdate()
        bIDClosed = False
    End Sub
    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        bHoldIdentify = False
        'bflashid = False

        bIDClosed = True
        Me.Dispose()
    End Sub

    Private Sub cbbSelectionField_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbbAttributeField.SelectedIndexChanged
        'MMaPPit.tsbcbbStandPoly.Text = cbbAttributeField.Text
        IdentifyField = cbbAttributeField.Text
        MMaPPit.PopulateIdentifyID(IdentifyField)
    End Sub

    Private Sub cbHoldIdentify_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbHoldIdentify.CheckedChanged
        bHoldIdentify = cbHoldIdentify.Checked
        bFlashID = bHoldIdentify
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        IdentifyOpacity -= 10
        IdentifyOpacity = Math.Max(0, IdentifyOpacity)
        lblOpacity.Text = IdentifyOpacity
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        IdentifyOpacity += 10
        IdentifyOpacity = Math.Min(255, IdentifyOpacity)
        lblOpacity.Text = IdentifyOpacity
    End Sub
End Class
