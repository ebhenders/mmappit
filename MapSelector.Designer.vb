<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MapSelector
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblSelectorText = New System.Windows.Forms.Label
        Me.tbStandID = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.clbSelector = New System.Windows.Forms.CheckedListBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.cbbSelectionField = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.Opacity = New System.Windows.Forms.Label
        Me.lblOpacity = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblSelectorText
        '
        Me.lblSelectorText.AutoSize = True
        Me.lblSelectorText.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectorText.Location = New System.Drawing.Point(37, 8)
        Me.lblSelectorText.Name = "lblSelectorText"
        Me.lblSelectorText.Size = New System.Drawing.Size(159, 23)
        Me.lblSelectorText.TabIndex = 0
        Me.lblSelectorText.Text = "Stand ID of Polygon"
        '
        'tbStandID
        '
        Me.tbStandID.Location = New System.Drawing.Point(12, 109)
        Me.tbStandID.Name = "tbStandID"
        Me.tbStandID.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.tbStandID.Size = New System.Drawing.Size(259, 20)
        Me.tbStandID.TabIndex = 1
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(115, 135)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(71, 32)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Select"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(208, 219)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(76, 32)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Cancel"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'clbSelector
        '
        Me.clbSelector.CheckOnClick = True
        Me.clbSelector.FormattingEnabled = True
        Me.clbSelector.Location = New System.Drawing.Point(12, 181)
        Me.clbSelector.Name = "clbSelector"
        Me.clbSelector.Size = New System.Drawing.Size(190, 364)
        Me.clbSelector.TabIndex = 4
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(211, 175)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(60, 38)
        Me.Button4.TabIndex = 6
        Me.Button4.Text = "Get Values"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(33, 135)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(76, 32)
        Me.Button5.TabIndex = 7
        Me.Button5.Text = "Clear"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'cbbSelectionField
        '
        Me.cbbSelectionField.FormattingEnabled = True
        Me.cbbSelectionField.Location = New System.Drawing.Point(12, 66)
        Me.cbbSelectionField.Name = "cbbSelectionField"
        Me.cbbSelectionField.Size = New System.Drawing.Size(182, 21)
        Me.cbbSelectionField.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 16)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Selection Field"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(109, 16)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Selection Values"
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button6.Location = New System.Drawing.Point(253, 82)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(15, 18)
        Me.Button6.TabIndex = 44
        Me.Button6.Text = "^"
        Me.Button6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(50, Byte))
        Me.Button7.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button7.Location = New System.Drawing.Point(232, 82)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(15, 18)
        Me.Button7.TabIndex = 43
        Me.Button7.Text = "v"
        Me.Button7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Opacity
        '
        Me.Opacity.AutoSize = True
        Me.Opacity.Location = New System.Drawing.Point(204, 66)
        Me.Opacity.Name = "Opacity"
        Me.Opacity.Size = New System.Drawing.Size(43, 13)
        Me.Opacity.TabIndex = 45
        Me.Opacity.Text = "Opacity"
        '
        'lblOpacity
        '
        Me.lblOpacity.AutoSize = True
        Me.lblOpacity.Location = New System.Drawing.Point(250, 66)
        Me.lblOpacity.Name = "lblOpacity"
        Me.lblOpacity.Size = New System.Drawing.Size(25, 13)
        Me.lblOpacity.TabIndex = 46
        Me.lblOpacity.Text = "100"
        '
        'MapSelector
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(318, 557)
        Me.Controls.Add(Me.lblOpacity)
        Me.Controls.Add(Me.Opacity)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbbSelectionField)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.clbSelector)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbStandID)
        Me.Controls.Add(Me.lblSelectorText)
        Me.Name = "MapSelector"
        Me.Text = "Selector"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblSelectorText As System.Windows.Forms.Label
    Friend WithEvents tbStandID As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents clbSelector As System.Windows.Forms.CheckedListBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents cbbSelectionField As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Opacity As System.Windows.Forms.Label
    Friend WithEvents lblOpacity As System.Windows.Forms.Label
End Class
