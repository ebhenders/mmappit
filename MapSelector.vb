Public Class MapSelector
    Dim MaxItems As Integer
    Dim OldQueryField As String
    Dim CurrQueryField As String
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        RunSelection()
    End Sub
    Private Sub RunSelection()
        Dim arr() As String
        arr = Split(tbStandID.Text, ",")
        NSelectedID = arr.Length
        ReDim SelectedID(NSelectedID)
        For j As Integer = 1 To NSelectedID
            SelectedID(j) = arr(j - 1)
        Next
        SelectOpacity = CType(lblOpacity.Text, Integer)
        MMaPPit.DoSelect()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        bSelectorShowing = False
        Me.Close()
    End Sub

    Private Sub Selector_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bSelectorShowing = True
        For jatt As Integer = 1 To NumAtts
            cbbSelectionField.Items.Add(AttName(jatt))
        Next
        OldQueryField = QueryField
        If QueryField = "" Then QueryField = DisplayField
        If SelectOpacity <> 0 Then lblOpacity.Text = SelectOpacity
        SelectOpacity = CType(lblOpacity.Text, Integer)

        CurrQueryField = QueryField
        cbbSelectionField.Text = QueryField
        MMaPPit.PopulateQueryID(QueryField)
        lblSelectorText.Text = SelectorText
        clbSelector.Items.Clear()
        NSelectorItems = 0
        ReDim SelectorItems(NSelectorItems)
        MaxItems = 500
        MMaPPit.PopSelectorItems(MaxItems)

        For i As Integer = 1 To NSelectorItems
            If SelectorItems(i) <> "" Then clbSelector.Items.Add(SelectorItems(i))
        Next

     


    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbStandID.KeyPress
        If e.KeyChar = Chr(13) Then 'Chr(13) is the Enter Key
            'Runs the Button1_Click Event
            Button1_Click(Me, EventArgs.Empty)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim dummy As String = ""
        If clbSelector.CheckedItems.Count = 0 Then Exit Sub
        'If Trim(tbStandID.Text) <> "" Then dummy = tbStandID.Text & ","
        For Each o As Object In clbSelector.CheckedItems
            dummy = dummy & o & ","
        Next
        dummy = dummy.Substring(0, dummy.Length - 1)

        tbStandID.Text = dummy 'tbStandID.Text & "," & dummy
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'need to populate the list of potential values to select
        MaxItems += 500
        NSelectorItems = 0
        clbSelector.Items.Clear()
        ReDim SelectorItems(NSelectorItems)
        MMaPPit.PopSelectorItems(MaxItems)

        For i As Integer = 1 To NSelectorItems
            If SelectorItems(i) <> "" Then clbSelector.Items.Add(SelectorItems(i))
        Next
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        ClearSelection()
    End Sub
    Private Sub ClearSelection()
        tbStandID.Text = ""
        For i As Integer = 0 To clbSelector.Items.Count - 1
            clbSelector.SetItemChecked(i, False)
        Next
    End Sub

    Private Sub cbbSelectionField_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbbSelectionField.SelectedIndexChanged
        OldQueryField = QueryField
        QueryField = cbbSelectionField.Text
        CurrQueryField = QueryField
        If OldQueryField <> CurrQueryField Then
            ClearSelection()
            RunSelection()
        End If
        'MMaPPit.tsbcbbStandPoly.Text = cbbSelectionField.Text
        MMaPPit.PopulateQueryID(QueryField)
        lblSelectorText.Text = SelectorText
        clbSelector.Items.Clear()
        NSelectorItems = 0
        ReDim SelectorItems(NSelectorItems)
        MaxItems = 500
        MMaPPit.PopSelectorItems(MaxItems)

        For i As Integer = 1 To NSelectorItems
            If SelectorItems(i) <> "" Then clbSelector.Items.Add(SelectorItems(i))
        Next
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        SelectOpacity -= 10
        SelectOpacity = Math.Max(0, SelectOpacity)
        lblOpacity.Text = SelectOpacity
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        SelectOpacity += 10
        SelectOpacity = Math.Min(255, SelectOpacity)
        lblOpacity.Text = SelectOpacity
    End Sub

    Private Sub clbSelector_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clbSelector.SelectedIndexChanged
        Dim dummy As String = ""
        If clbSelector.CheckedItems.Count = 0 Then
            tbStandID.Text = dummy 'tbStandID.Text & "," & dummy
            Exit Sub
        End If

        'If Trim(tbStandID.Text) <> "" Then dummy = tbStandID.Text & ","
        For Each o As Object In clbSelector.CheckedItems
            dummy = dummy & o & ","
        Next
        dummy = dummy.Substring(0, dummy.Length - 1)

        tbStandID.Text = dummy 'tbStandID.Text & "," & dummy
    End Sub

    Private Sub Selector_Close(ByVal sender As System.Object, ByVal e As FormClosingEventArgs) Handles MyBase.FormClosing
        bSelectorShowing = False
    End Sub
End Class