Public Class ColorPicker



    Private Sub FireEventColors_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        bColorSaved = False
        If ChangeColors.Length = 1 Then
            tbColorLabel.Text = ChangeColors(0).LabelDispStr
            cbbMin.Text = ChangeColors(0).LabelMinVal
            cbbMax.Text = ChangeColors(0).LabelMaxVal
            tbFEr.Text = ChangeColors(0).Color.R.ToString
            tbFEg.Text = ChangeColors(0).Color.G.ToString
            tbFEb.Text = ChangeColors(0).Color.B.ToString
            pbFE.BackColor = ChangeColors(0).Color
            For jcolor As Integer = 1 To NUniqueValues
                cbbMin.Items.Add(Unique_Values(jcolor))
                cbbMax.Items.Add(Unique_Values(jcolor))
            Next
        Else
            tbColorLabel.Text = "Multiple Values"
            cbbMin.Text = "N/A" 'ChangeColors(0).LabelMinVal
            cbbMax.Text = "N/A" 'ChangeColors(0).LabelMaxVal
            tbFEr.Text = ChangeColors(0).Color.R.ToString
            tbFEg.Text = ChangeColors(0).Color.G.ToString
            tbFEb.Text = ChangeColors(0).Color.B.ToString
            pbFE.BackColor = ChangeColors(0).Color
            'For jcolor As Integer = 1 To NUniqueValues
            '    cbbMin.Items.Add(Unique_Values(jcolor))
            '    cbbMax.Items.Add(Unique_Values(jcolor))
            'Next
        End If

    End Sub

    Private Sub tbFEr_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFEr.Enter, tbFEr.Leave
        ' Dim arr As Integer
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        r = tbFEr.Text
        If r < 0 Then r = 0
        If r > 255 Then r = 255
        tbFEr.Text = r
        g = tbFEg.Text
        If g < 0 Then g = 0
        If g > 255 Then g = 255
        tbFEg.Text = g
        b = tbFEb.Text
        If b < 0 Then b = 0
        If b > 255 Then b = 255
        tbFEb.Text = b
        For jcolr As Integer = 0 To ChangeColors.Length - 1
            ChangeColors(jcolr).Color = Color.FromArgb(255, r, g, b)
        Next jcolr
        pbFE.BackColor = ChangeColors(0).Color
    End Sub

    Private Sub tbFEg_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFEg.Enter, tbFEg.Leave
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        r = tbFEr.Text
        If r < 0 Then r = 0
        If r > 255 Then r = 255
        tbFEr.Text = r
        g = tbFEg.Text
        If g < 0 Then g = 0
        If g > 255 Then g = 255
        tbFEg.Text = g
        b = tbFEb.Text
        If b < 0 Then b = 0
        If b > 255 Then b = 255
        tbFEb.Text = b
        For jcolr As Integer = 0 To ChangeColors.Length - 1
            ChangeColors(jcolr).Color = Color.FromArgb(255, r, g, b)
        Next jcolr
        pbFE.BackColor = ChangeColors(0).Color
    End Sub

    Private Sub tbFEb_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFEb.Enter, tbFEb.Leave
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        r = tbFEr.Text
        If r < 0 Then r = 0
        If r > 255 Then r = 255
        tbFEr.Text = r
        g = tbFEg.Text
        If g < 0 Then g = 0
        If g > 255 Then g = 255
        tbFEg.Text = g
        b = tbFEb.Text
        If b < 0 Then b = 0
        If b > 255 Then b = 255
        tbFEb.Text = b
        For jcolr As Integer = 0 To ChangeColors.Length - 1
            ChangeColors(jcolr).Color = Color.FromArgb(255, r, g, b)
        Next jcolr
        pbFE.BackColor = ChangeColors(0).Color
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        bCancel = True
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ChangeColors.Length = 1 Then
            ChangeColors(0).LabelDispStr = tbColorLabel.Text
            ChangeColors(0).LabelMinVal = cbbMin.Text
            ChangeColors(0).LabelMaxVal = cbbMax.Text
        End If
        bCancel = False
        bColorSaved = True
        Me.Close()
    End Sub
    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If bColorSaved = False Then bCancel = True
    End Sub

    Private Sub pbFE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbFE.Click

        Dim MyDialog As New ColorDialog
        If MyDialog.ShowDialog = System.Windows.Forms.DialogResult.Cancel Then Return
        For jcolr As Integer = 0 To ChangeColors.Length - 1
            ChangeColors(jcolr).Color = MyDialog.Color
        Next jcolr
        tbFEr.Text = ChangeColors(0).Color.R
        tbFEg.Text = ChangeColors(0).Color.G
        tbFEb.Text = ChangeColors(0).Color.B
        pbFE.BackColor = ChangeColors(0).Color
        MyDialog = Nothing
    End Sub
    Private Sub pbFE_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbFE.MouseHover

        pbFE.Cursor = Cursors.Hand

    End Sub

    Private Sub cbbMin_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbbMin.SelectedIndexChanged
        If CType(cbbMax.Text, Single) < CType(cbbMin.Text, Single) Then cbbMax.Text = cbbMin.Text
    End Sub

    Private Sub cbbMax_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbbMax.SelectedIndexChanged
        If CType(cbbMax.Text, Single) < CType(cbbMin.Text, Single) Then cbbMax.Text = cbbMin.Text
    End Sub
End Class